webpackJsonp([1,16],{

/***/ 606:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_routing__ = __webpack_require__(678);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__register_register_component__ = __webpack_require__(654);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__lock_lock_component__ = __webpack_require__(651);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__login_login_component__ = __webpack_require__(652);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__welcome_welcome_component__ = __webpack_require__(656);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__shared_big_footer_big_footer_module__ = __webpack_require__(367);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__user_activation_user_activation_component__ = __webpack_require__(655);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_http__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__forget_password_forget_password_component__ = __webpack_require__(650);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagesModule", function() { return PagesModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var PagesModule = (function () {
    function PagesModule() {
    }
    PagesModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["b" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["a" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_4__pages_routing__["a" /* PagesRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_9__shared_big_footer_big_footer_module__["a" /* BigFooterModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_http__["a" /* HttpModule */],
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_5__register_register_component__["a" /* RegisterComponent */],
                __WEBPACK_IMPORTED_MODULE_6__lock_lock_component__["a" /* LockComponent */],
                __WEBPACK_IMPORTED_MODULE_8__welcome_welcome_component__["a" /* WelcomeComponent */],
                __WEBPACK_IMPORTED_MODULE_10__user_activation_user_activation_component__["a" /* UserActivationComponent */],
                __WEBPACK_IMPORTED_MODULE_12__forget_password_forget_password_component__["a" /* ForgetPasswordComponent */]
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], PagesModule);
    return PagesModule;
}());
//# sourceMappingURL=pages.module.js.map

/***/ }),

/***/ 650:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service_service__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__register_password_validation__ = __webpack_require__(653);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgetPasswordComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ForgetPasswordComponent = (function () {
    function ForgetPasswordComponent(route, router, authServ, fb) {
        this.route = route;
        this.router = router;
        this.authServ = authServ;
        this.fb = fb;
        this.errorChangePassword = false;
        this.formSetNewPassword = this.fb.group({
            newPassword: ['', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].minLength(3)]],
            newConfirmPassword: ['', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].minLength(3)]]
        }, {
            validator: __WEBPACK_IMPORTED_MODULE_4__register_password_validation__["a" /* PasswordValidation */].MatchNewPassword // your validation method
        });
    }
    ForgetPasswordComponent.prototype.checkFullPageBackgroundImage = function () {
        var $page = $('.full-page');
        var image_src = $page.data('image');
        if (image_src !== undefined) {
            var image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>';
            $page.append(image_container);
        }
    };
    ;
    ForgetPasswordComponent.prototype.ngOnInit = function () {
        this.checkFullPageBackgroundImage();
        setTimeout(function () {
            // after 1000 ms we add the class animated to the loginUser/register card
            $('.card').removeClass('card-hidden');
        }, 700);
        this.email = this.route.snapshot.params['email'];
        this.verifyToken = this.route.snapshot.params['verifyToken'];
    };
    ForgetPasswordComponent.prototype.saveNewPassword = function () {
        var _this = this;
        this.authServ.forgetPassSetNew(this.email, this.formSetNewPassword.value.password, this.verifyToken)
            .subscribe(function (res) {
            if (!res.error) {
                swal({
                    type: 'success',
                    title: 'Success Save',
                    text: 'Your New Password is save !',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
                _this.router.navigate(['/pages']);
            }
        }, function (error) {
            var err = error.json();
            _this.errorChangePassword = err.error;
        });
    };
    ForgetPasswordComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["e" /* Component */])({
            selector: 'app-forget-password',
            template: __webpack_require__(712),
            styles: [__webpack_require__(686)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_auth_service_service__["a" /* AuthServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__services_auth_service_service__["a" /* AuthServiceService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__angular_forms__["d" /* FormBuilder */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_forms__["d" /* FormBuilder */]) === 'function' && _d) || Object])
    ], ForgetPasswordComponent);
    return ForgetPasswordComponent;
    var _a, _b, _c, _d;
}());
//# sourceMappingURL=forget-password.component.js.map

/***/ }),

/***/ 651:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service_service__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(364);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LockComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LockComponent = (function () {
    function LockComponent(fb, route, router, authServ) {
        this.fb = fb;
        this.route = route;
        this.router = router;
        this.authServ = authServ;
        this.resetPassword = false;
        this.resetQRCode = false;
        this.errorLogin = false;
        this.formResetPassword = this.fb.group({
            emailPassword: ['', [
                    __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].required,
                    __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].minLength(3),
                    __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].pattern('[a-zA-Z0-9._\-]+@[a-zA-Z0-9.\-]+\.[a-zA-Z]{2,4}')]],
        });
        this.formResetQR = this.fb.group({
            emailQR: ['', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].required,
                    __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].minLength(3),
                    __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].pattern('[a-zA-Z0-9._\-]+@[a-zA-Z0-9.\-]+\.[a-zA-Z]{2,4}')]],
        });
    }
    LockComponent.prototype.checkFullPageBackgroundImage = function () {
        var $page = $('.full-page');
        var image_src = $page.data('image');
        if (image_src !== undefined) {
            var image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>';
            $page.append(image_container);
        }
    };
    ;
    LockComponent.prototype.ngOnInit = function () {
        console.log();
        if (this.route.snapshot.params['status'] === 'resetQR') {
            this.resetQRCode = true;
        }
        else if (this.route.snapshot.params['status'] === 'forgetPassword') {
            this.resetPassword = true;
        }
        this.checkFullPageBackgroundImage();
        setTimeout(function () {
            // after 1000 ms we add the class animated to the loginUser/register card
            $('.card').removeClass('card-hidden');
        }, 700);
    };
    LockComponent.prototype.resetQrCode = function () {
        var _this = this;
        this.authServ.resetQr(this.formResetQR.value.emailQR).subscribe(function (res) {
            if (!res.error) {
                swal({
                    type: 'success',
                    title: 'Reset Success',
                    text: 'Please Login, and scan new qr bar code!',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
                _this.router.navigate(['/pages/login']);
            }
        }, function (err) {
            _this.errorLogin = true;
        });
    };
    LockComponent.prototype.foregPassword = function () {
        var _this = this;
        console.log(this.formResetPassword.value.emailPassword);
        this.authServ.forgetPassword(this.formResetPassword.value.emailPassword).subscribe(function (res) {
            if (!res.error) {
                swal({
                    type: 'success',
                    title: 'Reset Mail Send In Your Address',
                    text: 'Check the email, and change password.',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
                _this.router.navigate(['/pages']);
            }
        }, function (err) {
            _this.errorLogin = true;
        });
    };
    LockComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["e" /* Component */])({
            selector: 'app-lock-cmp',
            template: __webpack_require__(713),
            styles: [__webpack_require__(687)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__angular_forms__["d" /* FormBuilder */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_forms__["d" /* FormBuilder */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services_auth_service_service__["a" /* AuthServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__services_auth_service_service__["a" /* AuthServiceService */]) === 'function' && _d) || Object])
    ], LockComponent);
    return LockComponent;
    var _a, _b, _c, _d;
}());
//# sourceMappingURL=lock.component.js.map

/***/ }),

/***/ 652:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service_service__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(35);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginComponent = (function () {
    function LoginComponent(fb, authService, router) {
        this.fb = fb;
        this.authService = authService;
        this.router = router;
        this.test = new Date();
        this.errorLogin = false;
        this.successLogin = false;
        this.formLogin = this.fb.group({
            email: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].pattern('[a-zA-Z0-9._\-]+@[a-zA-Z0-9.\-]+\.[a-zA-Z]{2,4}')]],
            password: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].minLength(1), __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].maxLength(30)]],
        });
        this.formLoginSecureCode = this.fb.group({
            secureCode1: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].maxLength(1)]],
            secureCode2: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].maxLength(1)]],
            secureCode3: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].maxLength(1)]],
            secureCode4: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].maxLength(1)]],
            secureCode5: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].maxLength(1)]],
            secureCode6: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].maxLength(1)]]
        });
    }
    LoginComponent.prototype.checkFullPageBackgroundImage = function () {
        var $page = $('.full-page');
        var image_src = $page.data('image');
        if (image_src !== undefined) {
            var image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>';
            $page.append(image_container);
        }
    };
    ;
    LoginComponent.prototype.ngOnInit = function () {
        this.checkFullPageBackgroundImage();
        setTimeout(function () {
            // after 1000 ms we add the class animated to the loginUser/register card
            $('.card').removeClass('card-hidden');
        }, 300);
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.authService.loginUser(this.formLogin.value.email, this.formLogin.value.password)
            .subscribe(function (res) {
            if (!res.error) {
                if (_this.errorLogin) {
                    _this.errorLogin = res.error;
                    _this.successLogin = true;
                }
                if (res.message === 'new_qrCode') {
                    _this.imageQRcode = res.imageSrc;
                    _this.secCode = res.secureCode;
                    $('#popUpQrCode').modal('show');
                    _this.clearSuccessAnimationInModel();
                }
                else {
                    _this.openPoUpSecureCode();
                }
            }
        }, function (err) {
            var er = err.json();
            _this.errorLogin = er.error;
        });
    };
    LoginComponent.prototype.clearSuccessAnimationInModel = function () {
        $('.my-success-modal.my-success-modal-show').removeClass('my-success-modal-show');
        $('.my-header-link-app.my-header-link-app-hide').removeClass('my-header-link-app-hide');
        setTimeout(function () {
            $('.my-success-modal').addClass('my-success-modal-hide');
            $('.my-header-link-app').addClass('my-header-link-app-show');
        }, 1000);
    };
    LoginComponent.prototype.saveQRCode = function () {
        var _this = this;
        $('#popUpQrCode').modal('hide');
        this.authService.loginSaveQRCodeUser(this.formLogin.value.email).subscribe(function (res) {
            if (!res.error) {
                _this.openPoUpSecureCode();
            }
        });
    };
    LoginComponent.prototype.openPoUpSecureCode = function () {
        var _this = this;
        $('#popUpInputSecureCode').modal('show');
        setTimeout(function () {
            _this.inputAutomaticNext();
        }, 100);
    };
    LoginComponent.prototype.inputAutomaticNext = function () {
        $('#secureCode1').focus();
        this.formLoginSecureCode.controls['secureCode1'].valueChanges.subscribe(function (value) {
            if (value.length > 0) {
                $('#secureCode2').focus();
            }
        });
        this.formLoginSecureCode.controls['secureCode2'].valueChanges.subscribe(function (value) {
            if (value.length > 0) {
                $('#secureCode3').focus();
            }
        });
        this.formLoginSecureCode.controls['secureCode3'].valueChanges.subscribe(function (value) {
            if (value.length > 0) {
                $('#secureCode4').focus();
            }
        });
        this.formLoginSecureCode.controls['secureCode4'].valueChanges.subscribe(function (value) {
            if (value.length > 0) {
                $('#secureCode5').focus();
            }
        });
        this.formLoginSecureCode.controls['secureCode5'].valueChanges.subscribe(function (value) {
            if (value.length > 0) {
                $('#secureCode6').focus();
            }
        });
    };
    LoginComponent.prototype.secureCodeCompleteInFormControls = function () {
        this.successSecureCode = this.formLoginSecureCode.controls['secureCode1'].value
            + this.formLoginSecureCode.controls['secureCode2'].value
            + this.formLoginSecureCode.controls['secureCode3'].value
            + this.formLoginSecureCode.controls['secureCode4'].value
            + this.formLoginSecureCode.controls['secureCode5'].value
            + this.formLoginSecureCode.controls['secureCode6'].value;
        return this.successSecureCode;
    };
    LoginComponent.prototype.loginCheckSecureCode = function () {
        var _this = this;
        this.authService.loginSecureCodeFromQRCodeUser(this.formLogin.value.email, this.secureCodeCompleteInFormControls())
            .subscribe(function (res) {
            if (!res.error) {
                _this.authService.saveJWTokenInLocalStorage(res.tokenJWT);
                _this.authService.saveAuthorInLocalStorage(res.username);
                $('.row-google-auth-image').css('display', 'none');
                $('.my-error-modal.my-error-modal-show').removeClass('my-error-modal-show');
                $('.my-error-modal').addClass('my-error-modal-hide');
                $('#success-popupSecureCode.my-success-modal.my-success-modal-hide').removeClass('my-success-modal-hide');
                $('#success-popupSecureCode.my-success-modal').addClass('my-success-modal-show');
                $('#popUpInputSecureCode').modal('hide');
                _this.router.navigate(['/dashboard']);
            }
        }, function (err) {
            var er = err.json();
            if (er.error) {
                $('.row-google-auth-image').css('display', 'none');
                $('.my-error-modal.my-error-modal-hide').removeClass('my-error-modal-hide');
                $('.my-error-modal').addClass('my-error-modal-show');
            }
        });
    };
    LoginComponent.prototype.resetQrCode = function () {
        $('#popUpInputSecureCode').modal('hide');
        this.router.navigate(['/pages/lock', { status: 'resetQR' }]);
    };
    LoginComponent.prototype.forgetPassword = function () {
        this.router.navigate(['/pages/lock', { status: 'forgetPassword' }]);
    };
    LoginComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["e" /* Component */])({
            selector: 'app-login-cmp',
            template: __webpack_require__(714),
            styles: [__webpack_require__(688)],
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormBuilder */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormBuilder */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_auth_service_service__["a" /* AuthServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__services_auth_service_service__["a" /* AuthServiceService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === 'function' && _c) || Object])
    ], LoginComponent);
    return LoginComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ 653:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordValidation; });
var PasswordValidation = (function () {
    function PasswordValidation() {
    }
    PasswordValidation.MatchPassword = function (AC) {
        var password = AC.get('password').value; // to get value in input tag
        var confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
        if (password !== confirmPassword) {
            // console.log('false');
            AC.get('confirmPassword').setErrors({ MatchPassword: true });
        }
        else {
            // console.log('true');
            AC.get('confirmPassword').setErrors({ MatchPassword: false });
            return null;
        }
    };
    PasswordValidation.MatchNewPassword = function (AC) {
        var password = AC.get('newPassword').value; // to get value in input tag
        var confirmPassword = AC.get('newConfirmPassword').value; // to get value in input tag
        if (password !== confirmPassword) {
            // console.log('false');
            AC.get('newConfirmPassword').setErrors({ MatchNewPassword: true });
        }
        else {
            // console.log('true');
            AC.get('newConfirmPassword').setErrors({ MatchNewPassword: false });
            return null;
        }
    };
    return PasswordValidation;
}());
//# sourceMappingURL=password-validation.js.map

/***/ }),

/***/ 654:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__password_validation__ = __webpack_require__(653);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_service_service__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__nameCountries__ = __webpack_require__(677);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RegisterComponent = (function () {
    function RegisterComponent(fb, authService, router) {
        this.fb = fb;
        this.authService = authService;
        this.router = router;
        this.date = new Date();
        this.defaultAvatarImg = '../../assets/img/default-avatar.png';
        this.objectKeys = Object.keys;
        this.countryName = __WEBPACK_IMPORTED_MODULE_5__nameCountries__["a" /* Countries */];
        this.countryPhone = __WEBPACK_IMPORTED_MODULE_5__nameCountries__["b" /* CountriesPhoneCode */];
        this.formRegister = this.fb.group({
            givenName: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].required],
            familyName: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].required],
            birthday: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].required],
            city: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].required],
            state: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].required],
            username: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].required],
            phone: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].required],
            password: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].required],
            confirmPassword: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* Validators */].required],
        }, {
            validator: __WEBPACK_IMPORTED_MODULE_2__password_validation__["a" /* PasswordValidation */].MatchPassword // your validation method
        });
    }
    RegisterComponent.prototype.checkFullPageBackgroundImage = function () {
        var $page = $('.full-page');
        var image_src = $page.data('image');
        if (image_src !== undefined) {
            var image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>';
            $page.append(image_container);
        }
    };
    ;
    RegisterComponent.prototype.keys = function (object) {
        return Object.keys(object);
    };
    RegisterComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                _this.location = position.coords;
                console.log(position.coords);
            });
        }
        this.checkFullPageBackgroundImage();
        // Code for the Validator
        var $validator = $('.wizard-card form').validate({
            rules: {
                firstname: {
                    required: true,
                    minlength: 3
                },
                lastname: {
                    required: true,
                    minlength: 3
                },
                email: {
                    required: true,
                    minlength: 3,
                },
                password: {
                    required: true,
                    minlength: 1,
                },
                city: {
                    required: true
                },
                country: {
                    required: true
                },
                birthday: {
                    required: true
                }
            },
            errorPlacement: function (error, element) {
                $(element).parent('div').addClass('has-error');
            }
        });
        // Wizard Initialization
        $('.wizard-card').bootstrapWizard({
            'tabClass': 'nav nav-pills',
            'nextSelector': '.btn-next',
            'previousSelector': '.btn-previous',
            onNext: function (tab, navigation, index) {
                var $valid = $('.wizard-card form').valid();
                if (!$valid) {
                    $validator.focusInvalid();
                    return false;
                }
            },
            onInit: function (tab, navigation, index) {
                // check number of tabs and fill the entire row
                var $total = navigation.find('li').length;
                var $width = 100 / $total;
                var $wizard = navigation.closest('.wizard-card');
                var $display_width = $(document).width();
                if ($display_width < 600 && $total > 3) {
                    $width = 50;
                }
                navigation.find('li').css('width', $width + '%');
                var $first_li = navigation.find('li:first-child a').html();
                var $moving_div = $('<div class="moving-tab" style="border-radius:0 !important;">' + $first_li + '</div>');
                $('.wizard-card .wizard-navigation').append($moving_div);
                //    this.refreshAnimation($wizard, index);
                var total_steps = $wizard.find('li').length;
                var move_distance = $wizard.width() / total_steps;
                var step_width = move_distance;
                move_distance *= index;
                var $current = index + 1;
                if ($current === 1) {
                    move_distance -= 8;
                }
                else if ($current === total_steps) {
                    move_distance += 8;
                }
                $wizard.find('.moving-tab').css('width', step_width);
                $('.moving-tab').css({
                    'transform': 'translate3d(' + move_distance + 'px, 0, 0)',
                    'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'
                });
                $('.moving-tab').css('transition', 'transform 0s');
            },
            onTabClick: function (tab, navigation, index) {
                return $('.wizard-card form').valid();
            },
            onTabShow: function (tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index + 1;
                var $wizard = navigation.closest('.wizard-card');
                // If it's the last tab then hide the last button and show the finish instead
                if ($current >= $total) {
                    $($wizard).find('.btn-next').hide();
                    $($wizard).find('.btn-finish').show();
                }
                else {
                    $($wizard).find('.btn-next').show();
                    $($wizard).find('.btn-finish').hide();
                }
                var button_text = navigation.find('li:nth-child(' + $current + ') a').html();
                setTimeout(function () {
                    $('.moving-tab').text(button_text);
                }, 150);
                var checkbox = $('.footer-checkbox');
                if (index !== 0) {
                    $(checkbox).css({
                        'opacity': '0',
                        'visibility': 'hidden',
                        'position': 'absolute'
                    });
                }
                else {
                    $(checkbox).css({
                        'opacity': '1',
                        'visibility': 'visible'
                    });
                }
                // this.refreshAnimation($wizard, index);
                var total_steps = $wizard.find('li').length;
                var move_distance = $wizard.width() / total_steps;
                var step_width = move_distance;
                move_distance *= index;
                $current = index + 1;
                if ($current === 1) {
                    move_distance -= 8;
                }
                else if ($current === total_steps) {
                    move_distance += 8;
                }
                $wizard.find('.moving-tab').css('width', step_width);
                $('.moving-tab').css({
                    'transform': 'translate3d(' + move_distance + 'px, 0, 0)',
                    'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'
                });
            }
        });
        $('.set-full-height').css('height', 'auto');
    };
    RegisterComponent.prototype.onFileChange = function (fileInput) {
        var _this = this;
        this.defaultAvatarImg = fileInput.target.files[0];
        this.inputFile = fileInput.target.files[0];
        var reader = new FileReader();
        reader.onload = function (e) {
            _this.defaultAvatarImg = e.target.result;
        };
        reader.readAsDataURL(fileInput.target.files[0]);
    };
    RegisterComponent.prototype.ngOnChanges = function () {
    };
    RegisterComponent.prototype.ngAfterViewInit = function () {
        $('.wizard-card').each(function () {
            var $wizard = $(this);
            var index = $wizard.bootstrapWizard('currentIndex');
            // this.refreshAnimation($wizard, index);
            var total_steps = $wizard.find('li').length;
            var move_distance = $wizard.width() / total_steps;
            var step_width = move_distance;
            move_distance *= index;
            var $current = index + 1;
            if ($current === 1) {
                move_distance -= 8;
            }
            else if ($current === total_steps) {
                move_distance += 8;
            }
            $wizard.find('.moving-tab').css('width', step_width);
            $('.moving-tab').css({
                'transform': 'translate3d(' + move_distance + 'px, 0, 0)',
                'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'
            });
            $('.moving-tab').css({
                'transition': 'transform 0s'
            });
        });
    };
    RegisterComponent.prototype.register = function () {
        var _this = this;
        this.authService.registerUser(this.formRegister.value).subscribe(function (res) {
            console.log(res);
            // if (!res['error']) {
            //     if (res['message'] === 'success_user_created') {
            $('.wizard-card').css('display', 'none');
            _this.authService.uploadAvatar(_this.inputFile, _this.formRegister.controls['username'].value)
                .subscribe(function (res1) {
                // console.log(res1);
            });
            _this.router.navigate(['/pages']);
            swal({
                type: 'success',
                title: 'Success Register',
                text: 'You are successfully registered. Check the email, and confirm the registration.',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
            //     }
            // }
        });
    };
    RegisterComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* Pipe */])({ name: 'keys' }),
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["e" /* Component */])({
            selector: 'app-register-cmp',
            template: __webpack_require__(715),
            styles: [__webpack_require__(689)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormBuilder */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormBuilder */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services_auth_service_service__["a" /* AuthServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__services_auth_service_service__["a" /* AuthServiceService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* Router */]) === 'function' && _c) || Object])
    ], RegisterComponent);
    return RegisterComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=register.component.js.map

/***/ }),

/***/ 655:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service_service__ = __webpack_require__(219);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserActivationComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserActivationComponent = (function () {
    function UserActivationComponent(route, router, authServ) {
        this.route = route;
        this.router = router;
        this.authServ = authServ;
    }
    UserActivationComponent.prototype.checkFullPageBackgroundImage = function () {
        var $page = $('.full-page');
        var image_src = $page.data('image');
        if (image_src !== undefined) {
            var image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>';
            $page.append(image_container);
        }
    };
    ;
    UserActivationComponent.prototype.ngOnInit = function () {
        var _this = this;
        // console.log(this.route.snapshot.params);
        this.email = this.route.snapshot.params['email'];
        this.verifyToken = this.route.snapshot.params['verifyToken'];
        this.authServ.registerVerifyEmailTokenUser(this.route.snapshot.params).subscribe(function (res) {
            console.log(res);
            if (res['error'] === false) {
                if (res['message'] === 'success_user_verify') {
                    _this.router.navigate(['/pages/login']);
                    swal({
                        type: 'success',
                        title: 'Success Register',
                        text: 'You email is confirm.',
                        buttonsStyling: false,
                        confirmButtonClass: 'btn btn-success'
                    });
                }
                else {
                }
            }
        });
        this.checkFullPageBackgroundImage();
        setTimeout(function () {
            // after 1000 ms we add the class animated to the loginUser/register card
            $('.card').removeClass('card-hidden');
        }, 700);
    };
    UserActivationComponent.prototype.goToLogin = function () {
        this.router.navigate(['/pages/login']);
    };
    UserActivationComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["e" /* Component */])({
            selector: 'app-user-activation',
            template: __webpack_require__(716),
            styles: [__webpack_require__(690)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_auth_service_service__["a" /* AuthServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__services_auth_service_service__["a" /* AuthServiceService */]) === 'function' && _c) || Object])
    ], UserActivationComponent);
    return UserActivationComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=user-activation.component.js.map

/***/ }),

/***/ 656:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WelcomeComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WelcomeComponent = (function () {
    function WelcomeComponent() {
        this.test = new Date();
    }
    WelcomeComponent.prototype.checkFullPageBackgroundImage = function () {
        var $page = $('.full-page');
        var image_src = $page.data('image');
        if (image_src !== undefined) {
            var image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>';
            $page.append(image_container);
        }
    };
    ;
    WelcomeComponent.prototype.ngOnInit = function () {
        this.checkFullPageBackgroundImage();
        setTimeout(function () {
            // after 1000 ms we add the class animated to the loginUser/register card
            $('.card').removeClass('card-hidden');
        }, 700);
    };
    WelcomeComponent.prototype.scrollDown = function () {
        $('html, body').animate({ scrollTop: $('#myContentWelcome').offset().top }, 800);
    };
    WelcomeComponent.prototype.toTop = function () {
        $('html, body').animate({ scrollTop: $('.wrapper').offset().top }, 800);
    };
    WelcomeComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["e" /* Component */])({
            selector: 'app-welcome-cmp',
            styles: [__webpack_require__(691)],
            template: __webpack_require__(717)
        }), 
        __metadata('design:paramtypes', [])
    ], WelcomeComponent);
    return WelcomeComponent;
}());
//# sourceMappingURL=welcome.component.js.map

/***/ }),

/***/ 677:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Countries; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return CountriesPhoneCode; });
/**
 * Created by Mita on 24.9.2017..
 */
var Countries = {
    'BD': 'Bangladesh',
    'BE': 'Belgium',
    'BF': 'Burkina Faso',
    'BG': 'Bulgaria',
    'BA': 'Bosnia and Herzegovina',
    'BB': 'Barbados',
    'WF': 'Wallis and Futuna',
    'BL': 'Saint Barthelemy',
    'BM': 'Bermuda',
    'BN': 'Brunei',
    'BO': 'Bolivia',
    'BH': 'Bahrain',
    'BI': 'Burundi',
    'BJ': 'Benin',
    'BT': 'Bhutan',
    'JM': 'Jamaica',
    'BV': 'Bouvet Island',
    'BW': 'Botswana',
    'WS': 'Samoa',
    'BQ': 'Bonaire, Saint Eustatius and Saba ',
    'BR': 'Brazil',
    'BS': 'Bahamas',
    'JE': 'Jersey',
    'BY': 'Belarus',
    'BZ': 'Belize',
    'RU': 'Russia',
    'RW': 'Rwanda',
    'RS': 'Serbia',
    'TL': 'East Timor',
    'RE': 'Reunion',
    'TM': 'Turkmenistan',
    'TJ': 'Tajikistan',
    'RO': 'Romania',
    'TK': 'Tokelau',
    'GW': 'Guinea-Bissau',
    'GU': 'Guam',
    'GT': 'Guatemala',
    'GS': 'South Georgia and the South Sandwich Islands',
    'GR': 'Greece',
    'GQ': 'Equatorial Guinea',
    'GP': 'Guadeloupe',
    'JP': 'Japan',
    'GY': 'Guyana',
    'GG': 'Guernsey',
    'GF': 'French Guiana',
    'GE': 'Georgia',
    'GD': 'Grenada',
    'GB': 'United Kingdom',
    'GA': 'Gabon',
    'SV': 'El Salvador',
    'GN': 'Guinea',
    'GM': 'Gambia',
    'GL': 'Greenland',
    'GI': 'Gibraltar',
    'GH': 'Ghana',
    'OM': 'Oman',
    'TN': 'Tunisia',
    'JO': 'Jordan',
    'HR': 'Croatia',
    'HT': 'Haiti',
    'HU': 'Hungary',
    'HK': 'Hong Kong',
    'HN': 'Honduras',
    'HM': 'Heard Island and McDonald Islands',
    'VE': 'Venezuela',
    'PR': 'Puerto Rico',
    'PS': 'Palestinian Territory',
    'PW': 'Palau',
    'PT': 'Portugal',
    'SJ': 'Svalbard and Jan Mayen',
    'PY': 'Paraguay',
    'IQ': 'Iraq',
    'PA': 'Panama',
    'PF': 'French Polynesia',
    'PG': 'Papua New Guinea',
    'PE': 'Peru',
    'PK': 'Pakistan',
    'PH': 'Philippines',
    'PN': 'Pitcairn',
    'PL': 'Poland',
    'PM': 'Saint Pierre and Miquelon',
    'ZM': 'Zambia',
    'EH': 'Western Sahara',
    'EE': 'Estonia',
    'EG': 'Egypt',
    'ZA': 'South Africa',
    'EC': 'Ecuador',
    'IT': 'Italy',
    'VN': 'Vietnam',
    'SB': 'Solomon Islands',
    'ET': 'Ethiopia',
    'SO': 'Somalia',
    'ZW': 'Zimbabwe',
    'SA': 'Saudi Arabia',
    'ES': 'Spain',
    'ER': 'Eritrea',
    'ME': 'Montenegro',
    'MD': 'Moldova',
    'MG': 'Madagascar',
    'MF': 'Saint Martin',
    'MA': 'Morocco',
    'MC': 'Monaco',
    'UZ': 'Uzbekistan',
    'MM': 'Myanmar',
    'ML': 'Mali',
    'MO': 'Macao',
    'MN': 'Mongolia',
    'MH': 'Marshall Islands',
    'MK': 'Macedonia',
    'MU': 'Mauritius',
    'MT': 'Malta',
    'MW': 'Malawi',
    'MV': 'Maldives',
    'MQ': 'Martinique',
    'MP': 'Northern Mariana Islands',
    'MS': 'Montserrat',
    'MR': 'Mauritania',
    'IM': 'Isle of Man',
    'UG': 'Uganda',
    'TZ': 'Tanzania',
    'MY': 'Malaysia',
    'MX': 'Mexico',
    'IL': 'Israel',
    'FR': 'France',
    'IO': 'British Indian Ocean Territory',
    'SH': 'Saint Helena',
    'FI': 'Finland',
    'FJ': 'Fiji',
    'FK': 'Falkland Islands',
    'FM': 'Micronesia',
    'FO': 'Faroe Islands',
    'NI': 'Nicaragua',
    'NL': 'Netherlands',
    'NO': 'Norway',
    'NA': 'Namibia',
    'VU': 'Vanuatu',
    'NC': 'New Caledonia',
    'NE': 'Niger',
    'NF': 'Norfolk Island',
    'NG': 'Nigeria',
    'NZ': 'New Zealand',
    'NP': 'Nepal',
    'NR': 'Nauru',
    'NU': 'Niue',
    'CK': 'Cook Islands',
    'XK': 'Kosovo',
    'CI': 'Ivory Coast',
    'CH': 'Switzerland',
    'CO': 'Colombia',
    'CN': 'China',
    'CM': 'Cameroon',
    'CL': 'Chile',
    'CC': 'Cocos Islands',
    'CA': 'Canada',
    'CG': 'Republic of the Congo',
    'CF': 'Central African Republic',
    'CD': 'Democratic Republic of the Congo',
    'CZ': 'Czech Republic',
    'CY': 'Cyprus',
    'CX': 'Christmas Island',
    'CR': 'Costa Rica',
    'CW': 'Curacao',
    'CV': 'Cape Verde',
    'CU': 'Cuba',
    'SZ': 'Swaziland',
    'SY': 'Syria',
    'SX': 'Sint Maarten',
    'KG': 'Kyrgyzstan',
    'KE': 'Kenya',
    'SS': 'South Sudan',
    'SR': 'Suriname',
    'KI': 'Kiribati',
    'KH': 'Cambodia',
    'KN': 'Saint Kitts and Nevis',
    'KM': 'Comoros',
    'ST': 'Sao Tome and Principe',
    'SK': 'Slovakia',
    'KR': 'South Korea',
    'SI': 'Slovenia',
    'KP': 'North Korea',
    'KW': 'Kuwait',
    'SN': 'Senegal',
    'SM': 'San Marino',
    'SL': 'Sierra Leone',
    'SC': 'Seychelles',
    'KZ': 'Kazakhstan',
    'KY': 'Cayman Islands',
    'SG': 'Singapore',
    'SE': 'Sweden',
    'SD': 'Sudan',
    'DO': 'Dominican Republic',
    'DM': 'Dominica',
    'DJ': 'Djibouti',
    'DK': 'Denmark',
    'VG': 'British Virgin Islands',
    'DE': 'Germany',
    'YE': 'Yemen',
    'DZ': 'Algeria',
    'US': 'United States',
    'UY': 'Uruguay',
    'YT': 'Mayotte',
    'UM': 'United States Minor Outlying Islands',
    'LB': 'Lebanon',
    'LC': 'Saint Lucia',
    'LA': 'Laos',
    'TV': 'Tuvalu',
    'TW': 'Taiwan',
    'TT': 'Trinidad and Tobago',
    'TR': 'Turkey',
    'LK': 'Sri Lanka',
    'LI': 'Liechtenstein',
    'LV': 'Latvia',
    'TO': 'Tonga',
    'LT': 'Lithuania',
    'LU': 'Luxembourg',
    'LR': 'Liberia',
    'LS': 'Lesotho',
    'TH': 'Thailand',
    'TF': 'French Southern Territories',
    'TG': 'Togo',
    'TD': 'Chad',
    'TC': 'Turks and Caicos Islands',
    'LY': 'Libya',
    'VA': 'Vatican',
    'VC': 'Saint Vincent and the Grenadines',
    'AE': 'United Arab Emirates',
    'AD': 'Andorra',
    'AG': 'Antigua and Barbuda',
    'AF': 'Afghanistan',
    'AI': 'Anguilla',
    'VI': 'U.S. Virgin Islands',
    'IS': 'Iceland',
    'IR': 'Iran',
    'AM': 'Armenia',
    'AL': 'Albania',
    'AO': 'Angola',
    'AQ': 'Antarctica',
    'AS': 'American Samoa',
    'AR': 'Argentina',
    'AU': 'Australia',
    'AT': 'Austria',
    'AW': 'Aruba',
    'IN': 'India',
    'AX': 'Aland Islands',
    'AZ': 'Azerbaijan',
    'IE': 'Ireland',
    'ID': 'Indonesia',
    'UA': 'Ukraine',
    'QA': 'Qatar',
    'MZ': 'Mozambique'
};
var CountriesPhoneCode = {
    'BD': '880',
    'BE': '32',
    'BF': '226',
    'BG': '359',
    'BA': '387',
    'BB': '1-246',
    'WF': '681',
    'BL': '590',
    'BM': '1-441',
    'BN': '673',
    'BO': '591',
    'BH': '973',
    'BI': '257',
    'BJ': '229',
    'BT': '975',
    'JM': '1-876',
    'BV': '',
    'BW': '267',
    'WS': '685',
    'BQ': '599',
    'BR': '55',
    'BS': '1-242',
    'JE': '44-1534',
    'BY': '375',
    'BZ': '501',
    'RU': '7',
    'RW': '250',
    'RS': '381',
    'TL': '670',
    'RE': '262',
    'TM': '993',
    'TJ': '992',
    'RO': '40',
    'TK': '690',
    'GW': '245',
    'GU': '1-671',
    'GT': '502',
    'GS': '',
    'GR': '30',
    'GQ': '240',
    'GP': '590',
    'JP': '81',
    'GY': '592',
    'GG': '44-1481',
    'GF': '594',
    'GE': '995',
    'GD': '1-473',
    'GB': '44',
    'GA': '241',
    'SV': '503',
    'GN': '224',
    'GM': '220',
    'GL': '299',
    'GI': '350',
    'GH': '233',
    'OM': '968',
    'TN': '216',
    'JO': '962',
    'HR': '385',
    'HT': '509',
    'HU': '36',
    'HK': '852',
    'HN': '504',
    'HM': ' ',
    'VE': '58',
    'PR': '1-787 and 1-939',
    'PS': '970',
    'PW': '680',
    'PT': '351',
    'SJ': '47',
    'PY': '595',
    'IQ': '964',
    'PA': '507',
    'PF': '689',
    'PG': '675',
    'PE': '51',
    'PK': '92',
    'PH': '63',
    'PN': '870',
    'PL': '48',
    'PM': '508',
    'ZM': '260',
    'EH': '212',
    'EE': '372',
    'EG': '20',
    'ZA': '27',
    'EC': '593',
    'IT': '39',
    'VN': '84',
    'SB': '677',
    'ET': '251',
    'SO': '252',
    'ZW': '263',
    'SA': '966',
    'ES': '34',
    'ER': '291',
    'ME': '382',
    'MD': '373',
    'MG': '261',
    'MF': '590',
    'MA': '212',
    'MC': '377',
    'UZ': '998',
    'MM': '95',
    'ML': '223',
    'MO': '853',
    'MN': '976',
    'MH': '692',
    'MK': '389',
    'MU': '230',
    'MT': '356',
    'MW': '265',
    'MV': '960',
    'MQ': '596',
    'MP': '1-670',
    'MS': '1-664',
    'MR': '222',
    'IM': '44-1624',
    'UG': '256',
    'TZ': '255',
    'MY': '60',
    'MX': '52',
    'IL': '972',
    'FR': '33',
    'IO': '246',
    'SH': '290',
    'FI': '358',
    'FJ': '679',
    'FK': '500',
    'FM': '691',
    'FO': '298',
    'NI': '505',
    'NL': '31',
    'NO': '47',
    'NA': '264',
    'VU': '678',
    'NC': '687',
    'NE': '227',
    'NF': '672',
    'NG': '234',
    'NZ': '64',
    'NP': '977',
    'NR': '674',
    'NU': '683',
    'CK': '682',
    'XK': '',
    'CI': '225',
    'CH': '41',
    'CO': '57',
    'CN': '86',
    'CM': '237',
    'CL': '56',
    'CC': '61',
    'CA': '1',
    'CG': '242',
    'CF': '236',
    'CD': '243',
    'CZ': '420',
    'CY': '357',
    'CX': '61',
    'CR': '506',
    'CW': '599',
    'CV': '238',
    'CU': '53',
    'SZ': '268',
    'SY': '963',
    'SX': '599',
    'KG': '996',
    'KE': '254',
    'SS': '211',
    'SR': '597',
    'KI': '686',
    'KH': '855',
    'KN': '1-869',
    'KM': '269',
    'ST': '239',
    'SK': '421',
    'KR': '82',
    'SI': '386',
    'KP': '850',
    'KW': '965',
    'SN': '221',
    'SM': '378',
    'SL': '232',
    'SC': '248',
    'KZ': '7',
    'KY': '1-345',
    'SG': '65',
    'SE': '46',
    'SD': '249',
    'DO': '1-809 and 1-829',
    'DM': '1-767',
    'DJ': '253',
    'DK': '45',
    'VG': '1-284',
    'DE': '49',
    'YE': '967',
    'DZ': '213',
    'US': '1',
    'UY': '598',
    'YT': '262',
    'UM': '1',
    'LB': '961',
    'LC': '1-758',
    'LA': '856',
    'TV': '688',
    'TW': '886',
    'TT': '1-868',
    'TR': '90',
    'LK': '94',
    'LI': '423',
    'LV': '371',
    'TO': '676',
    'LT': '370',
    'LU': '352',
    'LR': '231',
    'LS': '266',
    'TH': '66',
    'TF': '',
    'TG': '228',
    'TD': '235',
    'TC': '1-649',
    'LY': '218',
    'VA': '379',
    'VC': '1-784',
    'AE': '971',
    'AD': '376',
    'AG': '1-268',
    'AF': '93',
    'AI': '1-264',
    'VI': '1-340',
    'IS': '354',
    'IR': '98',
    'AM': '374',
    'AL': '355',
    'AO': '244',
    'AQ': '',
    'AS': '1-684',
    'AR': '54',
    'AU': '61',
    'AT': '43',
    'AW': '297',
    'IN': '91',
    'AX': '358-18',
    'AZ': '994',
    'IE': '353',
    'ID': '62',
    'UA': '380',
    'QA': '974',
    'MZ': '258'
};
//# sourceMappingURL=nameCountries.js.map

/***/ }),

/***/ 678:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__register_register_component__ = __webpack_require__(654);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__lock_lock_component__ = __webpack_require__(651);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login_component__ = __webpack_require__(652);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__welcome_welcome_component__ = __webpack_require__(656);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_activation_user_activation_component__ = __webpack_require__(655);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__forget_password_forget_password_component__ = __webpack_require__(650);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PagesRoutes; });






var PagesRoutes = [
    {
        path: '',
        children: [{
                path: '',
                component: __WEBPACK_IMPORTED_MODULE_3__welcome_welcome_component__["a" /* WelcomeComponent */]
            }, {
                path: 'login',
                component: __WEBPACK_IMPORTED_MODULE_2__login_login_component__["a" /* LoginComponent */]
            }, {
                path: 'lock',
                component: __WEBPACK_IMPORTED_MODULE_1__lock_lock_component__["a" /* LockComponent */]
            }, {
                path: 'register',
                component: __WEBPACK_IMPORTED_MODULE_0__register_register_component__["a" /* RegisterComponent */]
            }, {
                // http://localhost:4200/pages/userActivation/mitad92%40gmail.com/123456
                path: 'userActivation/:email/:verifyToken',
                component: __WEBPACK_IMPORTED_MODULE_4__user_activation_user_activation_component__["a" /* UserActivationComponent */]
            }, {
                // http://localhost:4200/pages/changePassword/mitad92@gmail.com/123456
                path: 'changePassword/:email/:verifyToken',
                component: __WEBPACK_IMPORTED_MODULE_5__forget_password_forget_password_component__["a" /* ForgetPasswordComponent */]
            }]
    }
];
//# sourceMappingURL=pages.routing.js.map

/***/ }),

/***/ 686:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(138)();
// imports


// module
exports.push([module.i, ".btn {\n  border-radius: 0 !important; }\n\n.card {\n  border-radius: 0 !important; }\n\n.my-alert-danger {\n  position: relative;\n  top: 250px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 687:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(138)();
// imports


// module
exports.push([module.i, ".btn {\n  border-radius: 0 !important; }\n\n.card {\n  border-radius: 0 !important; }\n\n.my-alert-danger {\n  position: relative;\n  top: 250px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 688:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(138)();
// imports


// module
exports.push([module.i, ".my-login-header {\n  background-color: #54a0fb !important;\n  border-radius: 0 !important; }\n\n.card-login {\n  border-radius: 0 !important; }\n\n.my-success-modal-show {\n  display: block; }\n\n.my-success-modal-hide {\n  display: none; }\n\n.my-error-modal-show {\n  display: block; }\n\n.my-error-modal-hide {\n  display: none; }\n\n.my-header-link-app-show {\n  display: block; }\n\n.my-header-link-app-hide {\n  display: none; }\n\n.my-text-size-12 {\n  font-size: 12px;\n  color: #54a0fb; }\n\n.row1 .col-md-6 {\n  text-align: center; }\n\n.my-row-center {\n  text-align: center; }\n\n.modal-content {\n  border-radius: 0 !important; }\n\n.btn-modal {\n  min-width: 110px; }\n\n#popUpInputSecureCode .form-control, #popUpInputSecureCode .form-group .form-control {\n  background-image: linear-gradient(#54a0fb, #54a0fb), linear-gradient(#54a0fb, #54a0fb) !important; }\n\n#popUpInputSecureCode .welcome-header {\n  margin-bottom: 20px; }\n\n#popUpInputSecureCode .welcome-strong {\n  font-size: 25px; }\n\n#popUpInputSecureCode .modal-content .modal-body {\n  padding-top: 0 !important; }\n\n#popUpInputSecureCode .form-group .input-secureCode:nth-child(6) {\n  margin-right: -10; }\n\n#popUpInputSecureCode .input-group {\n  display: block !important; }\n\n#popUpInputSecureCode .input-secureCode {\n  font-size: 30px;\n  width: 30px;\n  margin-right: 6px;\n  display: inline-block; }\n\n.btn-forget {\n  text-transform: none !important; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 689:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(138)();
// imports


// module
exports.push([module.i, ".label-birthday {\n  background-color: white !important; }\n\n.picture {\n  border-radius: 0 !important; }\n\n.my-wizard-card {\n  border-radius: 0 !important; }\n  .my-wizard-card .btn {\n    border-radius: 0 !important; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 690:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(138)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 691:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(138)();
// imports


// module
exports.push([module.i, ".header-big-text {\n  color: #fff;\n  text-transform: uppercase; }\n\n.header-small-text {\n  color: #fff;\n  text-transform: uppercase; }\n\n.header-image-btn-left {\n  position: relative;\n  right: -19px;\n  background-color: #59a3fc !important;\n  border-top-left-radius: 10px !important;\n  border-bottom-left-radius: 10px !important;\n  border-top-right-radius: 0 !important;\n  border-bottom-right-radius: 0 !important; }\n\n.header-image-btn-right {\n  position: relative;\n  left: -18px;\n  background-color: #3d92f9 !important;\n  border-top-left-radius: 0 !important;\n  border-bottom-left-radius: 0 !important;\n  border-top-right-radius: 10px !important;\n  border-bottom-right-radius: 10px !important; }\n\n.btn-header-or {\n  padding: 5px;\n  background-color: #0078b2 !important;\n  z-index: 9999999; }\n\n.parent-btn-header-or {\n  position: relative;\n  top: -53px;\n  left: -6px; }\n\n.top-margin-container {\n  margin-top: 100px; }\n\n@media (max-width: 424px) {\n  .header-big-text {\n    font-size: 35px; }\n  .header-small-text {\n    font-size: 18px; }\n  .header-image-btn-left {\n    padding: 10px 16px 10px 6px;\n    position: relative;\n    right: -19px;\n    background-color: #59a3fc !important;\n    border-top-left-radius: 10px !important;\n    border-bottom-left-radius: 10px !important;\n    border-top-right-radius: 0 !important;\n    border-bottom-right-radius: 0 !important; }\n  .header-image-btn-right {\n    padding: 10px 6px 10px 16px;\n    position: relative;\n    left: -18px;\n    background-color: #3d92f9 !important;\n    border-top-left-radius: 0 !important;\n    border-bottom-left-radius: 0 !important;\n    border-top-right-radius: 10px !important;\n    border-bottom-right-radius: 10px !important; }\n  .btn-header-or {\n    padding: 5px;\n    background-color: #0078b2 !important;\n    z-index: 9999999; } }\n\n@media (max-width: 340px) {\n  .header-big-text {\n    font-size: 35px; }\n  .header-small-text {\n    font-size: 12px; }\n  .header-image-btn-left {\n    padding: 10px 15px 10px 2px;\n    position: relative;\n    right: -19px;\n    background-color: #59a3fc !important;\n    border-top-left-radius: 10px !important;\n    border-bottom-left-radius: 10px !important;\n    border-top-right-radius: 0 !important;\n    border-bottom-right-radius: 0 !important; }\n  .header-image-btn-right {\n    padding: 10px 2px 10px 15px;\n    position: relative;\n    left: -18px;\n    background-color: #3d92f9 !important;\n    border-top-left-radius: 0 !important;\n    border-bottom-left-radius: 0 !important;\n    border-top-right-radius: 10px !important;\n    border-bottom-right-radius: 10px !important; }\n  .btn-header-or {\n    padding: 5px;\n    background-color: #0078b2 !important;\n    z-index: 9999999; } }\n\n.my-content img {\n  max-width: 100%; }\n\n.my-content .container-active-project-title {\n  color: #5a5a5a;\n  font-size: 45px; }\n\n.my-content .container-active-project-text {\n  margin-top: 30px;\n  color: #5a5a5a;\n  font-size: 18px; }\n\n.my-content .card {\n  border-radius: 0 !important;\n  margin-left: 0 !important;\n  margin-right: 0 !important; }\n  .my-content .card .card-title {\n    text-align: left !important; }\n  .my-content .card .card-description {\n    text-align: left !important; }\n  .my-content .card .div-votes {\n    padding: 0; }\n  .my-content .card .card-image {\n    border-radius: 0 !important;\n    cursor: pointer !important;\n    margin-left: 0 !important;\n    margin-right: 0 !important;\n    background-image: none !important;\n    background-color: #54a0fb !important;\n    height: 175px; }\n  .my-content .card .card-image img {\n    border-radius: 0 !important;\n    cursor: pointer !important;\n    -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 0 95%);\n    clip-path: polygon(0 0, 100% 0, 100% 100%, 0 95%); }\n  .my-content .card .img-votes {\n    background-image: url(" + __webpack_require__(724) + ");\n    background-repeat: no-repeat;\n    background-position: center;\n    background-size: contain;\n    height: 33px;\n    margin-top: 10px !important; }\n  .my-content .card .h4-votes-number {\n    padding-left: 2px;\n    float: left;\n    line-height: 20px;\n    font-weight: bold; }\n  .my-content .card .h4-votes-number span {\n    font-size: 12px;\n    font-weight: normal; }\n  .my-content .card .card .card-footer .btn {\n    margin-top: 15px !important;\n    margin-left: 5px !important; }\n  .my-content .card .card-footer {\n    margin-bottom: 0 !important; }\n    .my-content .card .card-footer .btn, .my-content .card .card-footer .btn-primary {\n      background-color: #e0e0e1 !important;\n      border-radius: 0 !important;\n      box-shadow: 0 2px 2px 0 rgba(224, 224, 225, 0.14), 0 3px 1px -2px rgba(224, 224, 225, 0.2), 0 1px 5px 0 rgba(224, 224, 225, 0.12);\n      color: #333; }\n  .my-content .card .card-author {\n    color: #999999; }\n  .my-content .card .card-author p {\n    margin: 0; }\n  .my-content .card .card-author-p1 {\n    margin-top: 25px !important; }\n  .my-content .card .img-logo-vote {\n    width: 70px;\n    height: 60px; }\n\n.blue-section {\n  background-color: #59a3fc !important;\n  padding: 50px; }\n  .blue-section img {\n    max-width: 100%; }\n  .blue-section .vertical-align {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n  .blue-section h2 {\n    color: #fff;\n    text-align: center; }\n    .blue-section h2 span {\n      font-weight: bold;\n      text-align: center; }\n  .blue-section .join-slack {\n    background-color: white !important;\n    color: #59a3fc !important; }\n\n.gray-section {\n  background-color: #f4f4f4 !important;\n  padding: 50px; }\n  .gray-section img {\n    max-width: 100%; }\n  .gray-section .vertical-align {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n  .gray-section h2 {\n    color: #5a5a5a;\n    text-align: left; }\n  .gray-section .img-mac {\n    position: relative; }\n  .gray-section .p {\n    color: #5a5a5a;\n    text-align: left; }\n\n.gray-top-footer-section {\n  background-color: #f4f4f4 !important;\n  padding: 50px 0 0 0; }\n  .gray-top-footer-section img {\n    max-width: 100%; }\n  .gray-top-footer-section .text-on-screen {\n    color: #fff; }\n  .gray-top-footer-section .div-text-on-screen {\n    padding-left: 200px !important;\n    padding-right: 200px !important;\n    position: relative;\n    bottom: -350px;\n    z-index: 99999;\n    text-align: center; }\n\n.to-top {\n  text-align: center;\n  background-color: #59a3fc;\n  width: 45px;\n  height: 45px;\n  position: fixed;\n  right: 20px;\n  bottom: 20px;\n  line-height: 50px;\n  cursor: pointer; }\n\n@media (max-width: 1050px) {\n  .gray-top-footer-section .div-text-on-screen {\n    padding-left: 200px !important;\n    padding-right: 200px !important;\n    position: relative;\n    bottom: -310px;\n    z-index: 99999;\n    text-align: center; } }\n\n@media (max-width: 950px) {\n  .gray-top-footer-section .div-text-on-screen {\n    position: relative;\n    bottom: -250px;\n    z-index: 99999;\n    text-align: center;\n    font-size: 10px; } }\n\n@media (max-width: 780px) {\n  .gray-top-footer-section .div-text-on-screen {\n    position: relative;\n    bottom: -250px;\n    z-index: 99999;\n    text-align: center;\n    font-size: 10px; } }\n\n@media (max-width: 600px) {\n  .gray-top-footer-section .div-text-on-screen {\n    display: none; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 712:
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper wrapper-full-page\">\n    <nav class=\"navbar navbar-primary navbar-transparent navbar-fixed-top\">\n        <div class=\"container\">\n            <div class=\"navbar-header\">\n                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#menu-example\">\n                    <span class=\"sr-only\">Toggle navigation</span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                </button>\n                <a class=\"navbar-brand\" href=\"/#/pages/welcome\">\n                    <img class=\"navbar-brand\" src=\"../../../assets/imgEmc2/Emc2MeLogo_xA0_Image_2_.png\"/>\n                </a>\n            </div>\n            <div class=\"collapse navbar-collapse\" id=\"menu-example\">\n                <ul class=\"nav navbar-nav navbar-right\">\n                    <li routerLinkActive=\"\">\n                        <a routerLink=\"/dashboard\" href=\"#/dashboard\">\n                            <i class=\"material-icons\">dashboard</i> Dashboard\n                        </a>\n                    </li>\n                    <li routerLinkActive=\"\" class=\"\">\n                        <a routerLink=\"/pages/register\" href=\"#/pages/register\">\n                            <i class=\"material-icons\">person_add</i> Register\n                        </a>\n                    </li>\n                    <li routerLinkActive=\"active\" class=\"\">\n                        <a routerLink=\"/pages/login\" href=\"#/pages/login\">\n                            <i class=\"material-icons\">fingerprint</i> Login\n                        </a>\n                    </li>\n                </ul>\n            </div>\n        </div>\n    </nav>\n    <div class=\"full-page lock-page\" filter-color=\"black\" data-image=\"../../../assets/imgEmc2/slider.png\">\n        <!--   you can change the color of the filter page using: data-color=\"blue | green | orange | red | purple\" -->\n\n        <div class=\"content\">\n            <div class=\"container\">\n                <div class=\"row\">\n                    <div class=\"col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3\">\n                        <form method=\"#\" action=\"#\" [formGroup]=\"formSetNewPassword\" novalidate>\n                            <div class=\"card card-login card-hidden\">\n                                <!--<div class=\"card-header text-center my-login-header\" data-background-color=\"\">-->\n                                <!--<h4 class=\"card-title\">Login</h4>-->\n                                <!--</div>-->\n                                <div class=\"card-content\">\n                                    <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                            <i class=\"material-icons\">lock_outline</i>\n                                            </span>\n                                        <div class=\"form-group label-floating\">\n                                            <label class=\"control-label\">Password\n                                                <small>(required)</small>\n                                            </label>\n                                            <input type=\"password\"\n                                                   formControlName=\"newPassword\" class=\"form-control\">\n                                        </div>\n                                    </div>\n                                    <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                            <i class=\"material-icons\">lock_outline</i>\n                                            </span>\n                                        <div class=\"form-group label-floating\">\n                                            <label class=\"control-label\">Confirm Password\n                                                <small>(required)</small>\n                                            </label>\n                                            <input formControlName=\"newConfirmPassword\" type=\"password\"\n                                                   class=\"form-control\">\n                                            <small class=\"text-danger\"\n                                                   *ngIf=\"formSetNewPassword.controls['newConfirmPassword'].errors.MatchNewPassword\">\n                                                Password mismatch\n                                            </small>\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"footer text-center\">\n                                    <button type=\"submit\"\n                                            [disabled]=\"!formSetNewPassword.valid && formSetNewPassword.controls['newConfirmPassword'].errors.MatchNewPassword\"\n                                            class=\"btn btn-rose btn-simple btn-wd btn-lg\"\n                                            (click)=\"saveNewPassword()\">Reset Password\n                                    </button>\n                                </div>\n                            </div>\n                        </form>\n                    </div>\n                    <div *ngIf=\"errorChangePassword\" class=\"col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3\">\n                        <div class=\"alert alert-danger\">\n                            <button aria-hidden=\"true\" class=\"close\" type=\"button\">\n                                <i class=\"material-icons\">close</i>\n                            </button>\n                            <span>\n                                <b> Wrong Your Token Expired </b> Try again, or Contact Us</span>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<app-big-footer></app-big-footer>\n"

/***/ }),

/***/ 713:
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper wrapper-full-page\">\n    <nav class=\"navbar navbar-primary navbar-transparent navbar-fixed-top\">\n        <div class=\"container\">\n            <div class=\"navbar-header\">\n                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#menu-example\">\n                    <span class=\"sr-only\">Toggle navigation</span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                </button>\n                <a class=\"navbar-brand\" href=\"/#/pages/welcome\">\n                    <img class=\"navbar-brand\" src=\"../../../assets/imgEmc2/Emc2MeLogo_xA0_Image_2_.png\"/>\n                </a>\n            </div>\n            <div class=\"collapse navbar-collapse\" id=\"menu-example\">\n                <ul class=\"nav navbar-nav navbar-right\">\n                    <li routerLinkActive=\"\">\n                        <a routerLink=\"/dashboard\" href=\"#/dashboard\">\n                            <i class=\"material-icons\">dashboard</i> Dashboard\n                        </a>\n                    </li>\n                    <li routerLinkActive=\"\" class=\"\">\n                        <a routerLink=\"/pages/register\" href=\"#/pages/register\">\n                            <i class=\"material-icons\">person_add</i> Register\n                        </a>\n                    </li>\n                    <li routerLinkActive=\"active\" class=\"\">\n                        <a routerLink=\"/pages/login\" href=\"#/pages/login\">\n                            <i class=\"material-icons\">fingerprint</i> Login\n                        </a>\n                    </li>\n                </ul>\n            </div>\n        </div>\n    </nav>\n    <div class=\"full-page lock-page\" filter-color=\"black\" data-image=\"../../../assets/imgEmc2/slider.png\">\n        <!--   you can change the color of the filter page using: data-color=\"blue | green | orange | red | purple\" -->\n        <div *ngIf=\"resetPassword\" class=\"content\">\n            <form method=\"#\" action=\"#\" [formGroup]=\"formResetPassword\">\n                <div class=\"card card-profile card-hidden\">\n                    <div class=\"card-content\">\n                        <h4 class=\"card-title\">Reset password</h4>\n                        <div class=\"form-group label-floating\">\n                            <label class=\"control-label\">Enter address</label>\n                            <input type=\"email\" class=\"form-control\" formControlName=\"emailPassword\">\n                        </div>\n                    </div>\n                    <div class=\"card-footer\">\n                        <button type=\"button\"\n                                class=\"btn btn-primary my-btn-without-radius\" (click)=\"foregPassword()\">Send reset email\n                        </button>\n                    </div>\n                </div>\n            </form>\n            <div *ngIf=\"errorLogin\" class=\"col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3 my-alert-danger\">\n                <div class=\"alert alert-danger\">\n                    <button aria-hidden=\"true\" class=\"close\" type=\"button\">\n                        <i class=\"material-icons\">close</i>\n                    </button>\n                    <span><b> Wrong, email no exists! </b></span>\n                </div>\n            </div>\n        </div>\n        <div *ngIf=\"resetQRCode\" class=\"content\">\n            <form method=\"#\" action=\"#\" [formGroup]=\"formResetQR\">\n                <div class=\"card card-profile card-hidden\">\n                    <div class=\"card-content\">\n                        <h4 class=\"card-title\">Reset QR Code</h4>\n                        <div class=\"form-group label-floating\">\n                            <label class=\"control-label\">Enter address</label>\n                            <input type=\"email\" class=\"form-control\" formControlName=\"emailQR\">\n                        </div>\n                    </div>\n                    <div class=\"card-footer\">\n                        <button type=\"button\"\n                                class=\"btn btn-next btn-fill btn-rose btn-wd\" (click)=\"resetQrCode()\">\n                            Reset QR Code\n                        </button>\n                    </div>\n                </div>\n            </form>\n            <div *ngIf=\"errorLogin\" class=\"col-md-2 col-sm-4 col-md-offset-5 col-sm-offset-4 my-alert-danger\">\n                <div class=\"alert alert-danger\">\n                    <button aria-hidden=\"true\" class=\"close\" type=\"button\">\n                        <i class=\"material-icons\">close</i>\n                    </button>\n                    <span><b> Wrong, email no exists! </b></span>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<app-big-footer></app-big-footer>\n"

/***/ }),

/***/ 714:
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper wrapper-full-page\">\n    <nav class=\"navbar navbar-primary navbar-transparent navbar-fixed-top\">\n        <div class=\"container\">\n            <div class=\"navbar-header\">\n                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#menu-example\">\n                    <span class=\"sr-only\">Toggle navigation</span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                </button>\n                <a class=\"navbar-brand\" routerLink=\"/pages\" href=\"#/pages\">\n                    <img class=\"navbar-brand\" src=\"../../../assets/imgEmc2/Emc2MeLogo_xA0_Image_2_.png\"/>\n                </a>\n            </div>\n            <div class=\"collapse navbar-collapse\" id=\"menu-example\">\n                <ul class=\"nav navbar-nav navbar-right\">\n                    <li routerLinkActive=\"\">\n                        <a routerLink=\"/dashboard\" href=\"#/dashboard\">\n                            <i class=\"material-icons\">dashboard</i> Dashboard\n                        </a>\n                    </li>\n                    <li routerLinkActive=\"\" class=\"\">\n                        <a routerLink=\"/pages/register\" href=\"#/pages/register\">\n                            <i class=\"material-icons\">person_add</i> Register\n                        </a>\n                    </li>\n                    <li routerLinkActive=\"active\" class=\"\">\n                        <a routerLink=\"/pages/login\" href=\"#/pages/login\">\n                            <i class=\"material-icons\">fingerprint</i> Login\n                        </a>\n                    </li>\n                </ul>\n            </div>\n        </div>\n    </nav>\n    <div class=\"full-page login-page\" filter-color=\"black\" data-image=\"../../../assets/imgEmc2/slider.png\">\n        <!--   you can change the color of the filter page using: data-color=\"blue | purple | green | orange | red | rose \" -->\n        <div class=\"content\">\n            <div class=\"container\">\n                <div class=\"row\">\n                    <div class=\"col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3\">\n                        <form method=\"#\" action=\"#\" [formGroup]=\"formLogin\" novalidate>\n                            <div class=\"card card-login card-hidden\">\n                                <div class=\"card-header text-center my-login-header\" data-background-color=\"\">\n                                    <h4 class=\"card-title\">Login</h4>\n                                </div>\n                                <div class=\"card-content\">\n                                    <div class=\"input-group\">\n                                        <span class=\"input-group-addon\">\n                                            <i class=\"material-icons\">email</i>\n                                        </span>\n                                        <div class=\"form-group label-floating\">\n                                            <label class=\"control-label\">Email address</label>\n                                            <input type=\"email\" class=\"form-control\" formControlName=\"email\">\n                                        </div>\n                                    </div>\n                                    <div class=\"input-group\">\n                                        <span class=\"input-group-addon\">\n                                            <i class=\"material-icons\">lock_outline</i>\n                                        </span>\n                                        <div class=\"form-group label-floating\">\n                                            <label class=\"control-label\">Password</label>\n                                            <input type=\"password\" class=\"form-control\" formControlName=\"password\">\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"btn btn-rose btn-simple btn-forget\" (click)=\"forgetPassword()\">Forget password?\n                                </div>\n                                <div class=\"footer text-center\">\n                                    <button type=\"submit\"\n                                            [disabled]=\"!formLogin.valid\"\n                                            class=\"btn btn-rose btn-simple btn-wd btn-lg\"\n                                            (click)=\"login()\">Let's go\n                                    </button>\n                                </div>\n                            </div>\n                        </form>\n                    </div>\n                    <div *ngIf=\"errorLogin\" class=\"col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3\">\n                        <div class=\"alert alert-danger\">\n                            <button aria-hidden=\"true\" class=\"close\" type=\"button\">\n                                <i class=\"material-icons\">close</i>\n                            </button>\n                            <span>\n                                <b> Wrong password </b> Try again.</span>\n                        </div>\n                    </div>\n                    <div *ngIf=\"successLogin\" class=\"col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3\">\n                        <div class=\"alert alert-success\">\n                            <button aria-hidden=\"true\" class=\"close\" type=\"button\">\n                                <i class=\"material-icons\">close</i>\n                            </button>\n                            <span>\n                                <b> Success</b> password is correct</span>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n\n<app-big-footer></app-big-footer>\n\n<div class=\"modal fade\" id=\"popUpQrCode\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"\n     aria-hidden=\"true\">\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <div class=\"container-fluid\">\n                    <div class=\"row\">\n                        <div class=\"swal2-icon swal2-success animate my-success-modal my-success-modal-show\"><span\n                                class=\"line tip animate-success-tip\">\n                        </span> <span\n                                class=\"line long animate-success-long\"></span>\n                            <div class=\"placeholder\"></div>\n                            <div class=\"fix\"></div>\n                        </div>\n                        <div class=\"col-md-12 my-header-link-app my-header-link-app-hide\">\n                            <div class=\"col-md-12 col-sm-12 text-center\">\n                                <h5>You mast us Google Authenticator</h5>\n                            </div>\n                            <div class=\"col-md-10 col-sm-10 col-md-offset-1 col-sm-offset-1 text-center\">\n                                <p class=\"my-text-size-12\">You must download application and instal on your mobile, scan\n                                    qr code, or input secret code, and input new code in new window.</p>\n                            </div>\n                            <div class=\"row row1\">\n                                <div class=\"col-md-6\">\n                                    <a class=\"btn btn-primary\"\n                                       href=\"https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en\">\n                                        <i class=\"material-icons\">android</i>\n                                    </a>\n                                </div>\n                                <div class=\"col-md-6\">\n                                    <a class=\"btn btn-primary\"\n                                       href=\"https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8\">\n                                        <i class=\"fa fa-apple\"></i>\n                                    </a>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"container-fluid\">\n                    <div class=\"row\">\n                        <div class=\"col-md-12 my-row-center\">\n                            <div class=\"col-md-12\">\n                                Scan QR Code\n                            </div>\n                            <div class=\"col-md-12\">\n                                <img [src]=\"imageQRcode\">\n                            </div>\n                            <div class=\"col-md-12\">\n                                or\n                            </div>\n                            <div class=\"col-md-12\">\n                                Enter a Provided Key\n                            </div>\n                            <div class=\"col-md-12\">\n                                {{secCode}}\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <div class=\"container-fluid\">\n                    <div class=\"row\">\n                        <div class=\"col-md-12 my-row-center\">\n                            <button type=\"button\" class=\"btn btn-secondary btn-modal\" data-dismiss=\"modal\">Close\n                            </button>\n                            <button type=\"button\" class=\"btn btn-primary btn-modal\" (click)=\"saveQRCode()\">Save</button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\"modal fade\" id=\"popUpInputSecureCode\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"\n     aria-hidden=\"true\">\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <div class=\"container-fluid\">\n                    <div class=\"row\">\n                        <div id=\"success-popupSecureCode\"\n                             class=\"swal2-icon swal2-success animate my-success-modal my-success-modal-hide\"><span\n                                class=\"line tip animate-success-tip\">\n                        </span> <span\n                                class=\"line long animate-success-long\"></span>\n                            <div class=\"placeholder\"></div>\n                            <div class=\"fix\"></div>\n                            <img src=\"../../../assets/imgEmc2/google_authenticator.png\"\n                                 class=\"img-responsive my-image-google-auth\">\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"swal2-icon swal2-error animate-error-icon my-error-modal my-error-modal-hide\">\n                            <span class=\"x-mark animate-x-mark\"><span class=\"line left\"></span>\n                                <span class=\"line right\"></span>\n                            </span>\n                            <img src=\"../../../assets/imgEmc2/google_authenticator.png\"\n                                 class=\"img-responsive my-image-google-auth\">\n                        </div>\n                    </div>\n                    <div class=\"row row-google-auth-image\">\n                        <div class=\"swal2-icon\">\n                            <img src=\"../../../assets/imgEmc2/google_authenticator.png\"\n                                 class=\"img-responsive my-image-google-auth\">\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"container-fluid\">\n                    <div class=\"row\">\n                        <div class=\"col-md-10 col-sm-10 col-md-offset-1 col-sm-offset-1 text-center  welcome-header\">\n                            <div class=\"row\">\n                                <p>\n                                    <strong class=\"welcome-strong\">Welcome</strong>\n                                    {{formLogin.value.email}}\n                                </p>\n                            </div>\n                        </div>\n                        <div class=\"col-md-10 col-sm-10 col-md-offset-1 col-sm-offset-1 text-center\">\n                            <p class=\"\"><strong>2-Step Verification</strong><br/>\n                                Enter one of your 6-digit codes</p>\n                        </div>\n                        <div class=\"col-md-12\">\n                            <form method=\"#\" action=\"#\" [formGroup]=\"formLoginSecureCode\" novalidate>\n                                <div class=\"row my-row-center\">\n                                    <div class=\"col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3\">\n                                        <div class=\"input-group\">\n                                            <div class=\"form-group input-group-secureCode\">\n                                                <input type=\"text\" id=\"secureCode1\"\n                                                       class=\"form-control text-center input-secureCode \"\n                                                       maxlength=\"1\" formControlName=\"secureCode1\"\n                                                       placeholder=\"\">\n                                                <input type=\"text\" id=\"secureCode2\"\n                                                       class=\"form-control text-center input-secureCode\"\n                                                       maxlength=\"1\" formControlName=\"secureCode2\"\n                                                       placeholder=\"\">\n                                                <input type=\"text\" id=\"secureCode3\"\n                                                       class=\"form-control text-center input-secureCode\"\n                                                       maxlength=\"1\" formControlName=\"secureCode3\"\n                                                       placeholder=\"\">\n                                                <input type=\"text\" id=\"secureCode4\"\n                                                       class=\"form-control text-center input-secureCode\"\n                                                       maxlength=\"1\" formControlName=\"secureCode4\"\n                                                       placeholder=\"\">\n                                                <input type=\"text\" id=\"secureCode5\"\n                                                       class=\"form-control text-center input-secureCode\"\n                                                       maxlength=\"1\" formControlName=\"secureCode5\"\n                                                       placeholder=\"\">\n                                                <input type=\"text\" id=\"secureCode6\"\n                                                       class=\"form-control text-center input-secureCode\"\n                                                       maxlength=\"1\" formControlName=\"secureCode6\"\n                                                       placeholder=\"\">\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <div class=\"container-fluid\">\n                    <div class=\"row\">\n                        <div class=\"col-md-12 my-row-center\">\n                            <button type=\"button\" class=\"btn btn-simple btn-modal\" (click)=\"resetQrCode()\">Reset QR Code\n                            </button>\n                            <button type=\"button\" class=\"btn btn-primary btn-modal\" (click)=\"loginCheckSecureCode()\">\n                                Login\n                            </button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ 715:
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper wrapper-full-page\">\n    <nav class=\"navbar navbar-primary navbar-transparent navbar-absolute\">\n        <div class=\"container\">\n            <div class=\"navbar-header\">\n                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#menu-example\">\n                    <span class=\"sr-only\">Toggle navigation</span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                </button>\n                <a class=\"navbar-brand\" routerLink=\"/pages\" href=\"#/pages\">\n                    <img class=\"navbar-brand\" src=\"../../../assets/imgEmc2/Emc2MeLogo_xA0_Image_2_.png\"/>\n                </a>\n            </div>\n            <div class=\"collapse navbar-collapse\" id=\"menu-example\">\n                <ul class=\"nav navbar-nav navbar-right\">\n                    <li routerLinkActive=\"\">\n                        <a routerLink=\"/dashboard\" href=\"#/dashboard\">\n                            <i class=\"material-icons\">dashboard</i> Dashboard\n                        </a>\n                    </li>\n                    <li routerLinkActive=\"active\" class=\" \">\n                        <a routerLink=\"/pages/register\" href=\"#/pages/register\">\n                            <i class=\"material-icons\">person_add</i> Register\n                        </a>\n                    </li>\n                    <li routerLinkActive=\"\" class=\"\">\n                        <a routerLink=\"/pages/login\" href=\"#/pages/login\">\n                            <i class=\"material-icons\">fingerprint</i> Login\n                        </a>\n                    </li>\n                    <!--<li class=\"\">-->\n                    <!--<a href=\"/pages/lock\">-->\n                    <!--<i class=\"material-icons\">lock_open</i> Lock-->\n                    <!--</a>-->\n                    <!--</li>-->\n                </ul>\n            </div>\n        </div>\n    </nav>\n    <div class=\"full-page register-page\" filter-color=\"black\" data-image=\"../../../assets/imgEmc2/slider.png\">\n        <div class=\"container\">\n            <div class=\"row\">\n                <div class=\"col-sm-8 col-sm-offset-2\">\n                    <!--      Wizard container        -->\n                    <div class=\"wizard-container\">\n                        <div class=\"card wizard-card my-wizard-card\" data-color=\"rose\" id=\"wizardProfile\">\n                            <form action=\"\" method=\"\" [formGroup]=\"formRegister\">\n                                <!--        You can switch \" data-color=\"purple\" \"  with one of the next bright colors: \"green\", \"orange\", \"red\", \"blue\"       -->\n                                <div class=\"wizard-header\">\n                                    <h3 class=\"wizard-title\">\n                                        Build Your Profile\n                                    </h3>\n                                    <h5>This information will let us know more about you.</h5>\n                                </div>\n                                <div class=\"wizard-navigation\">\n                                    <ul>\n                                        <li>\n                                            <a href=\"#about\" data-toggle=\"tab\">About</a>\n                                        </li>\n                                        <li>\n                                            <a href=\"#account\" data-toggle=\"tab\">Account</a>\n                                        </li>\n                                        <li>\n                                            <a href=\"#address\" data-toggle=\"tab\">Address</a>\n                                        </li>\n                                    </ul>\n                                </div>\n                                <div class=\"tab-content\">\n                                    <div class=\"tab-pane\" id=\"about\">\n                                        <div class=\"row\">\n                                            <h4 class=\"info-text\"> Let's start with the basic information (with\n                                                validation)</h4>\n                                            <div class=\"col-sm-4 col-sm-offset-1\">\n                                                <div class=\"picture-container\">\n                                                    <div class=\"picture\">\n                                                        <img [src]=\"defaultAvatarImg\"\n                                                             class=\"picture-src\" id=\"wizardPicturePreview\" title=\"\"/>\n                                                        <input type=\"file\" id=\"wizard-picture\"\n                                                               (change)=\"onFileChange($event)\">\n                                                    </div>\n                                                    <h6>Choose Picture</h6>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-sm-6\">\n                                                <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                                <i class=\"material-icons\">face</i>\n                                            </span>\n                                                    <div class=\"form-group label-floating\">\n                                                        <label class=\"control-label\">First Name\n                                                            <small>(required)</small>\n                                                        </label>\n                                                        <input name=\"firstname\" type=\"text\"\n                                                               formControlName=\"givenName\" class=\"form-control\">\n                                                    </div>\n                                                </div>\n                                                <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                                <i class=\"material-icons\">record_voice_over</i>\n                                            </span>\n                                                    <div class=\"form-group label-floating\">\n                                                        <label class=\"control-label\">Last Name\n                                                            <small>(required)</small>\n                                                        </label>\n                                                        <input name=\"lastname\" formControlName=\"familyName\"\n                                                               type=\"text\" class=\"form-control\">\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"tab-pane\" id=\"account\">\n                                        <div class=\"row\">\n                                            <div class=\"col-sm-12\">\n                                                <h4 class=\"info-text\"> Are you living in a nice area? </h4>\n                                            </div>\n                                            <div class=\"col-sm-6 col-sm-offset-3\">\n                                                <div class=\"form-group label-floating input-birthday\">\n                                                    <label class=\"control-label label-birthday\">Birthday\n                                                        <small>(required)</small>\n                                                    </label>\n                                                    <input type=\"date\" formControlName=\"birthday\" name=\"birthday\"\n                                                           class=\"form-control\" placeholder=\"mm/dd/yyyy\">\n                                                </div>\n                                            </div>\n                                            <div class=\"col-sm-5 col-sm-offset-1\">\n                                                <div class=\"form-group label-floating\">\n                                                    <label class=\"control-label\">City\n                                                        <small>(required)</small>\n                                                    </label>\n                                                    <input type=\"text\" class=\"form-control\" formControlName=\"city\"\n                                                           name=\"city\">\n                                                </div>\n                                            </div>\n                                            <div class=\"col-sm-5\">\n                                                <div class=\"form-group label-floating\">\n                                                    <label class=\"control-label\">Country\n                                                        <small>(required)</small>\n                                                    </label>\n                                                    <select name=\"country\" class=\"form-control\"\n                                                            formControlName=\"state\">\n                                                        <option disabled=\"\" selected=\"\"></option>\n                                                        <option *ngFor=\"let key of objectKeys(countryName)\"\n                                                                [value]=\"key\">{{countryName[key] + \" (+\" +\n                                                            countryPhone[key]+\")\"}}\n                                                        </option>\n                                                        <!---->\n                                                        <!--<option *ngFor=\"let key of keys(countryName)\">-->\n                                                        <!--{{countryName[key]}}-->\n                                                        <!--</option>-->\n\n                                                    </select>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"tab-pane\" id=\"address\">\n                                        <div class=\"row\">\n                                            <div class=\"col-sm-12\">\n                                                <h4 class=\"info-text\"></h4>\n                                            </div>\n                                            <div class=\"col-lg-10 col-lg-offset-1\">\n                                                <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                            <i class=\"material-icons\">email</i>\n                                            </span>\n                                                    <div class=\"form-group label-floating\">\n                                                        <label class=\"control-label\">Email\n                                                            <small>(required)</small>\n                                                        </label>\n                                                        <input name=\"email\" type=\"email\" formControlName=\"username\"\n                                                               class=\"form-control\">\n                                                    </div>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-lg-10 col-lg-offset-1\">\n                                                <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                            <i class=\"material-icons\">lock_outline</i>\n                                            </span>\n                                                    <div class=\"form-group label-floating is-focused\">\n                                                        <label class=\"control-label\">Phone\n                                                            <small>(required)</small>\n                                                        </label>\n                                                        <input name=\"phone\" type=\"tel\"\n                                                               id=\"phone\"\n                                                               [value]='\"+\"+countryPhone[formRegister.controls[\"state\"].value]'\n                                                               formControlName=\"phone\" class=\"form-control\"\n                                                               pattern=\"[7-9]{1}[0-9]{9}\">\n                                                    </div>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-lg-10 col-lg-offset-1\">\n                                                <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                            <i class=\"material-icons\">lock_outline</i>\n                                            </span>\n                                                    <div class=\"form-group label-floating\">\n                                                        <label class=\"control-label\">Password\n                                                            <small>(required)</small>\n                                                        </label>\n                                                        <input name=\"password\" type=\"password\"\n                                                               id=\"password\"\n                                                               formControlName=\"password\" class=\"form-control\">\n                                                    </div>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-lg-10 col-lg-offset-1\">\n                                                <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                            <i class=\"material-icons\">lock_outline</i>\n                                            </span>\n                                                    <div class=\"form-group label-floating\">\n                                                        <label class=\"control-label\">Confirm Password\n                                                            <small>(required)</small>\n                                                        </label>\n                                                        <input name=\"confirm_password\"\n                                                               id=\"confirmPassword\"\n                                                               formControlName=\"confirmPassword\" type=\"password\"\n                                                               class=\"form-control\">\n                                                        <small class=\"text-danger\"\n                                                               *ngIf=\"formRegister.controls['confirmPassword'].errors.MatchPassword\">\n                                                            Password mismatch\n                                                        </small>\n                                                    </div>\n                                                </div>\n                                            </div>\n\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"wizard-footer\">\n                                    <div class=\"pull-right\">\n                                        <input type='button' class='btn btn-next btn-fill btn-rose btn-wd' name='next'\n                                               value='Next'/>\n                                        <input type='button' class='btn btn-finish btn-fill btn-rose btn-wd'\n                                               name='finish' (click)=\"register()\" value='Finish'/>\n                                    </div>\n                                    <div class=\"pull-left\">\n                                        <input type='button' class='btn btn-previous btn-fill btn-default btn-wd'\n                                               name='previous' value='Previous'/>\n                                    </div>\n                                    <div class=\"clearfix\"></div>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n                    <!-- wizard container -->\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<app-big-footer></app-big-footer>"

/***/ }),

/***/ 716:
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper wrapper-full-page\">\n    <nav class=\"navbar navbar-primary navbar-transparent navbar-fixed-top\">\n        <div class=\"container\">\n            <div class=\"navbar-header\">\n                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#menu-example\">\n                    <span class=\"sr-only\">Toggle navigation</span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                </button>\n                <a class=\"navbar-brand\" routerLink=\"/pages\" href=\"#/pages\">\n                    <img class=\"navbar-brand\" src=\"../../../assets/imgEmc2/Emc2MeLogo_xA0_Image_2_.png\"/>\n                </a>\n            </div>\n            <div class=\"collapse navbar-collapse\" id=\"menu-example\">\n                <ul class=\"nav navbar-nav navbar-right\">\n                    <li routerLinkActive=\"active\">\n                        <a routerLink=\"/dashboard\" href=\"#/dashboard\">\n                            <i class=\"material-icons\">dashboard</i> Dashboard\n                        </a>\n                    </li>\n                    <li routerLinkActive=\"active\" class=\" active \">\n                        <a routerLink=\"/pages/register\" href=\"#/pages/register\">\n                            <i class=\"material-icons\">person_add</i> Register\n                        </a>\n                    </li>\n                    <li routerLinkActive=\"active\" class=\"\">\n                        <a routerLink=\"/pages/login\" href=\"#/pages/login\">\n                            <i class=\"material-icons\">fingerprint</i> Login\n                        </a>\n                    </li>\n                    <!--<li class=\"\">-->\n                    <!--<a href=\"/pages/lock\">-->\n                    <!--<i class=\"material-icons\">lock_open</i> Lock-->\n                    <!--</a>-->\n                    <!--</li>-->\n                </ul>\n            </div>\n        </div>\n    </nav>\n    <div class=\"full-page lock-page\" filter-color=\"black\" data-image=\"../../../assets/imgEmc2/slider.png\">\n        <!--   you can change the color of the filter page using: data-color=\"blue | green | orange | red | purple\" -->\n        <div class=\"content\">\n            <!--<form method=\"#\" action=\"#\">-->\n            <!--<div class=\"card card-profile card-hidden\">-->\n            <!--<div class=\"card-avatar\">-->\n            <!--<a href=\"#pablo\">-->\n            <!--<img class=\"avatar\" src=\"../assets/img/faces/avatar.jpg\" alt=\"...\">-->\n            <!--</a>-->\n            <!--</div>-->\n            <!--<div class=\"card-content\">-->\n            <!--<h4 class=\"card-title\">{{email}}</h4>-->\n\n            <!--</div>-->\n            <!--<div class=\"card-footer\">-->\n            <!--<p>-->\n            <!--Vas Nalog je Aktiviran !-->\n            <!--</p>-->\n            <!--</div>-->\n            <!--</div>-->\n            <!--</form>-->\n        </div>\n    </div>\n</div>\n<app-big-footer></app-big-footer>\n"

/***/ }),

/***/ 717:
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper wrapper-full-page\">\n    <nav class=\"navbar navbar-primary navbar-transparent navbar-fixed-top\">\n        <div class=\"container\">\n            <div class=\"navbar-header\">\n                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#menu-example\">\n                    <span class=\"sr-only\">Toggle navigation</span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                </button>\n                <a class=\"navbar-brand\" routerLink=\"/pages\" href=\"#/pages\">\n                    <img class=\"navbar-brand\" src=\"../../../assets/imgEmc2/Emc2MeLogo_xA0_Image_2_.png\"/>\n                </a>\n            </div>\n            <div class=\"collapse navbar-collapse\" id=\"menu-example\">\n                <ul class=\"nav navbar-nav navbar-right\">\n                    <li routerLinkActive=\"\">\n                        <a routerLink=\"/dashboard\" href=\"#/dashboard\">\n                            <i class=\"material-icons\">dashboard</i> Dashboard\n                        </a>\n                    </li>\n                    <li routerLinkActive=\"\" class=\"\">\n                        <a routerLink=\"/pages/register\" href=\"#/pages/register\">\n                            <i class=\"material-icons\">person_add</i> Register\n                        </a>\n                    </li>\n                    <li routerLinkActive=\"active\" class=\"\">\n                        <a routerLink=\"/pages/login\" href=\"#/pages/login\">\n                            <i class=\"material-icons\">fingerprint</i> Login\n                        </a>\n                    </li>\n                </ul>\n            </div>\n        </div>\n    </nav>\n    <div class=\"full-page login-page\" filter-color=\"black\" data-image=\"../../../assets/imgEmc2/slider.png\">\n        <!--   you can change the color of the filter page using: data-color=\"blue | purple | green | orange | red | rose \" -->\n        <div class=\"content\">\n            <div class=\"container top-margin-container\">\n                <div class=\"row\">\n                    <div class=\"col-md-12 col-sm-12 col-xs-12 text-center header-image-text\">\n                        <h1 class=\"header-big-text\">Einsteinium accelerator</h1>\n                        <h3 class=\"header-small-text \">HELP FUND THE NEXT WAVE OF SCIENTIFIC PROJECTS</h3>\n                    </div>\n                    <div class=\"col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1  text-center top-margin-container\">\n                        <button class=\"btn btn-primary header-image-btn-left\">Start Project</button>\n                        <div class=\"btn btn-round btn-header-or\">or</div>\n                        <button class=\"btn btn-primary header-image-btn-right\">Browse Project</button>\n                    </div>\n                    <div class=\"col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1  text-center top-margin-container\">\n                        <div class=\"col-md-12 text-center top-margin-container\">\n                            <div class=\"btn btn-simple\" (click)=\"scrollDown()\">\n                                <i style=\"font-size:40px; color: #fff\" class=\"fa\">&#xf107;</i>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div id=\"myContentWelcome\" class=\"my-content top-margin-container\">\n    <div class=\"container \">\n        <div class=\"row text-center\">\n            <div class=\"col-md-12\">\n                <h2>Active Project</h2>\n                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore\n                    et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>\n            </div>\n        </div>\n        <div class=\"row top-margin-container\">\n            <div class=\"col-md-10\"></div>\n            <div class=\"col-md-2\">\n                <div class=\"btn btn-round\">Filter by</div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-lg-3 col-md-4 col-sm-6\">\n                <div class=\"card card-product\">\n                    <div class=\"card-image\" data-header-animation=\"true\">\n                        <a href=\"#pablo\">\n                            <img class=\"img\"\n                                 src=\"https://localhost:80/api/file/avatars/1504701726416_google_authenticator.png\">\n                        </a>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"card-actions\">\n                            <button class=\"btn btn-primary btn-simple\">Read More</button>\n                        </div>\n                        <h4 class=\"card-title\">\n                            <a href=\"#pablo\">Cozy 5 Stars Apartment</a>\n                        </h4>\n                        <div class=\"card-description\">  <!--150 char-->\n                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt\n                            ut labore et dolore magna aliqua. Ut enim ad minim veniam.\n                        </div>\n                        <div class=\"row\">\n                            <div class=\"col-md-12 \">\n                                <div class=\"card-author text-left\">\n                                    <p class=\"card-author-p1\">Author</p>\n                                    <p class=\"\">Bruce Willis</p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"row\">\n                            <div class=\"col-md-2 col-sm-2 col-xs-3 img-votes\">\n                            </div>\n                            <div class=\"col-md-5 col-sm-5 col-xs-5 div-votes\">\n                                <h4 class=\"h4-votes-number\">98,231,1258\n                                    <span>VOTES</span>\n                                </h4>\n                            </div>\n                            <div class=\"col-md-5  col-sm-5 col-xs-4\">\n                                <button class=\"btn btn-primary\">VOTE</button>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-lg-3 col-md-4 col-sm-6\">\n                <div class=\"card card-product\">\n                    <div class=\"card-image\" data-header-animation=\"true\">\n                        <a href=\"#pablo\">\n                            <img class=\"img\"\n                                 src=\"https://localhost:80/api/file/avatars/1504701726416_google_authenticator.png\">\n                        </a>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"card-actions\">\n                            <button class=\"btn btn-primary btn-simple\">Read More</button>\n                        </div>\n                        <h4 class=\"card-title\">\n                            <a href=\"#pablo\">Cozy 5 Stars Apartment</a>\n                        </h4>\n                        <div class=\"card-description\">  <!--150 char-->\n                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt\n                            ut labore et dolore magna aliqua. Ut enim ad minim veniam.\n                        </div>\n                        <div class=\"row\">\n                            <div class=\"col-md-12 \">\n                                <div class=\"card-author text-left\">\n                                    <p class=\"card-author-p1\">Author</p>\n                                    <p class=\"\">Bruce Willis</p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"row\">\n                            <div class=\"col-md-2 col-sm-2 col-xs-3 img-votes\">\n                            </div>\n                            <div class=\"col-md-5 col-sm-5 col-xs-5 div-votes\">\n                                <h4 class=\"h4-votes-number\">98,231,1258\n                                    <span>VOTES</span>\n                                </h4>\n                            </div>\n                            <div class=\"col-md-5  col-sm-5 col-xs-4\">\n                                <button class=\"btn btn-primary\">VOTE</button>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-lg-3 col-md-4 col-sm-6\">\n                <div class=\"card card-product\">\n                    <div class=\"card-image\" data-header-animation=\"true\">\n                        <a href=\"#pablo\">\n                            <img class=\"img\" src=\"../assets/img/card-2.jpeg\">\n                        </a>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"card-actions\">\n                            <button class=\"btn btn-primary btn-simple\">Read More</button>\n                        </div>\n                        <h4 class=\"card-title\">\n                            <a href=\"#pablo\">Cozy 5 Stars Apartment</a>\n                        </h4>\n                        <div class=\"card-description\">  <!--150 char-->\n                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt\n                            ut labore et dolore magna aliqua. Ut enim ad minim veniam.\n                        </div>\n                        <div class=\"row\">\n                            <div class=\"col-md-12 \">\n                                <div class=\"card-author text-left\">\n                                    <p class=\"card-author-p1\">Author</p>\n                                    <p class=\"\">Bruce Willis</p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"row\">\n                            <div class=\"col-md-2 col-sm-2 col-xs-3 img-votes\">\n                            </div>\n                            <div class=\"col-md-5 col-sm-5 col-xs-5 div-votes\">\n                                <h4 class=\"h4-votes-number\">98,231,1258\n                                    <span>VOTES</span>\n                                </h4>\n                            </div>\n                            <div class=\"col-md-5  col-sm-5 col-xs-4\">\n                                <button class=\"btn btn-primary\">VOTE</button>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-lg-3 col-md-4 col-sm-6\">\n                <div class=\"card card-product\">\n                    <div class=\"card-image\" data-header-animation=\"true\">\n                        <a href=\"#pablo\">\n                            <img class=\"img\" src=\"../assets/img/card-2.jpeg\">\n                        </a>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"card-actions\">\n                            <button class=\"btn btn-primary btn-simple\">Read More</button>\n                        </div>\n                        <h4 class=\"card-title\">\n                            <a href=\"#pablo\">Cozy 5 Stars Apartment</a>\n                        </h4>\n                        <div class=\"card-description\">  <!--150 char-->\n                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt\n                            ut labore et dolore magna aliqua. Ut enim ad minim veniam.\n                        </div>\n                        <div class=\"row\">\n                            <div class=\"col-md-12 \">\n                                <div class=\"card-author text-left\">\n                                    <p class=\"card-author-p1\">Author</p>\n                                    <p class=\"\">Bruce Willis</p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"row\">\n                            <div class=\"col-md-2 col-sm-2 col-xs-3 img-votes\">\n                            </div>\n                            <div class=\"col-md-5 col-sm-5 col-xs-5 div-votes\">\n                                <h4 class=\"h4-votes-number\">98,231,1258\n                                    <span>VOTES</span>\n                                </h4>\n                            </div>\n                            <div class=\"col-md-5  col-sm-5 col-xs-4\">\n                                <button class=\"btn btn-primary\">VOTE</button>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-lg-3 col-md-4 col-sm-6\">\n                <div class=\"card card-product\">\n                    <div class=\"card-image\" data-header-animation=\"true\">\n                        <a href=\"#pablo\">\n                            <img class=\"img\" src=\"../assets/img/card-2.jpeg\">\n                        </a>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"card-actions\">\n                            <button class=\"btn btn-primary btn-simple\">Read More</button>\n                        </div>\n                        <h4 class=\"card-title\">\n                            <a href=\"#pablo\">Cozy 5 Stars Apartment</a>\n                        </h4>\n                        <div class=\"card-description\">  <!--150 char-->\n                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt\n                            ut labore et dolore magna aliqua. Ut enim ad minim veniam.\n                        </div>\n                        <div class=\"row\">\n                            <div class=\"col-md-12 \">\n                                <div class=\"card-author text-left\">\n                                    <p class=\"card-author-p1\">Author</p>\n                                    <p class=\"\">Bruce Willis</p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"row\">\n                            <div class=\"col-md-2 col-sm-2 col-xs-3 img-votes\">\n                            </div>\n                            <div class=\"col-md-5 col-sm-5 col-xs-5 div-votes\">\n                                <h4 class=\"h4-votes-number\">98,231,1258\n                                    <span>VOTES</span>\n                                </h4>\n                            </div>\n                            <div class=\"col-md-5  col-sm-5 col-xs-4\">\n                                <button class=\"btn btn-primary\">VOTE</button>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-lg-3 col-md-4 col-sm-6\">\n                <div class=\"card card-product\">\n                    <div class=\"card-image\" data-header-animation=\"true\">\n                        <a href=\"#pablo\">\n                            <img class=\"img\" src=\"../assets/img/card-2.jpeg\">\n                        </a>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"card-actions\">\n                            <button class=\"btn btn-primary btn-simple\">Read More</button>\n                        </div>\n                        <h4 class=\"card-title\">\n                            <a href=\"#pablo\">Cozy 5 Stars Apartment</a>\n                        </h4>\n                        <div class=\"card-description\">  <!--150 char-->\n                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt\n                            ut labore et dolore magna aliqua. Ut enim ad minim veniam.\n                        </div>\n                        <div class=\"row\">\n                            <div class=\"col-md-12 \">\n                                <div class=\"card-author text-left\">\n                                    <p class=\"card-author-p1\">Author</p>\n                                    <p class=\"\">Bruce Willis</p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"row\">\n                            <div class=\"col-md-2 col-sm-2 col-xs-3 img-votes\">\n                            </div>\n                            <div class=\"col-md-5 col-sm-5 col-xs-5 div-votes\">\n                                <h4 class=\"h4-votes-number\">98,231,1258\n                                    <span>VOTES</span>\n                                </h4>\n                            </div>\n                            <div class=\"col-md-5  col-sm-5 col-xs-4\">\n                                <button class=\"btn btn-primary\">VOTE</button>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-lg-3 col-md-4 col-sm-6\">\n                <div class=\"card card-product\">\n                    <div class=\"card-image\" data-header-animation=\"true\">\n                        <a href=\"#pablo\">\n                            <img class=\"img\" src=\"../assets/img/card-2.jpeg\">\n                        </a>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"card-actions\">\n                            <button class=\"btn btn-primary btn-simple\">Read More</button>\n                        </div>\n                        <h4 class=\"card-title\">\n                            <a href=\"#pablo\">Cozy 5 Stars Apartment</a>\n                        </h4>\n                        <div class=\"card-description\">  <!--150 char-->\n                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt\n                            ut labore et dolore magna aliqua. Ut enim ad minim veniam.\n                        </div>\n                        <div class=\"row\">\n                            <div class=\"col-md-12 \">\n                                <div class=\"card-author text-left\">\n                                    <p class=\"card-author-p1\">Author</p>\n                                    <p class=\"\">Bruce Willis</p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"row\">\n                            <div class=\"col-md-2 col-sm-2 col-xs-3 img-votes\">\n                            </div>\n                            <div class=\"col-md-5 col-sm-5 col-xs-5 div-votes\">\n                                <h4 class=\"h4-votes-number\">98,231,1258\n                                    <span>VOTES</span>\n                                </h4>\n                            </div>\n                            <div class=\"col-md-5  col-sm-5 col-xs-4\">\n                                <button class=\"btn btn-primary\">VOTE</button>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\" gray-section\">\n    <div class=\"container\">\n        <div class=\"row vertical-align\">\n            <div class=\"col-md-12 col-sm-12 col-xs-12\">\n                <div class=\"col-md-4 col-sm-12 col-xs-12\">\n                    <h2 class=\"text-left\">\n                        Your scientific discovery\n                        awaits, start now\n                    </h2>\n                    <div class=\"\">\n                        <p>\n                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt\n                            ut\n                            labore et dolore magna aliqua.\n                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea\n                            commodo\n                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu\n                            fugiat nulla pariatur.\n                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit\n                            anim\n                            id est laborum.\n                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque\n                            laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi\n                            architecto\n                            beatae vitae dicta sunt explicabo.\n                            Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia\n                            consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam\n                            est,\n                            qui dolorem ipsum quia dolor sit amet,\n                            consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et\n                            dolore\n                            magnam aliquam quaerat voluptatem.\n                        </p>\n                    </div>\n                </div>\n                <div class=\"col-md-8 col-sm-12 col-xs-12\">\n                    <img src=\"../../../assets/imgEmc2/macbook.png\" class=\" img-mac\">\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div id=\"myContentFoundedWelcome\" class=\"my-content top-margin-container\">\n    <div class=\"container \">\n        <div class=\"row text-center\">\n            <div class=\"col-md-12\">\n                <h2>Founded Project</h2>\n                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore\n                    et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>\n            </div>\n        </div>\n        <div class=\"row top-margin-container\">\n            <div class=\"col-md-10\"></div>\n            <div class=\"col-md-2\">\n                <div class=\"btn btn-round\">Filter by</div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-lg-3 col-md-4 col-sm-6\">\n                <div class=\"card card-product\">\n                    <div class=\"card-image\" data-header-animation=\"true\">\n                        <a href=\"#pablo\">\n                            <img class=\"img\" src=\"../assets/img/card-2.jpeg\">\n                        </a>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"card-actions\">\n                            <button class=\"btn btn-primary btn-simple\">Read More</button>\n                        </div>\n                        <h4 class=\"card-title\">\n                            <a href=\"#pablo\">Cozy 5 Stars Apartment</a>\n                        </h4>\n                        <div class=\"card-description\">  <!--150 char-->\n                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt\n                            ut labore et dolore magna aliqua. Ut enim ad minim veniam.\n                        </div>\n                        <div class=\"row\">\n                            <div class=\"col-md-12 \">\n                                <div class=\"card-author text-left\">\n                                    <p class=\"card-author-p1\">Author</p>\n                                    <p class=\"\">Bruce Willis</p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"row\">\n                            <div class=\"col-md-2 col-sm-2 col-xs-3 img-votes\">\n                            </div>\n                            <div class=\"col-md-5 col-sm-5 col-xs-5 div-votes\">\n                                <h4 class=\"h4-votes-number\">98,231,1258\n                                    <span>VOTES</span>\n                                </h4>\n                            </div>\n                            <div class=\"col-md-5  col-sm-5 col-xs-4\">\n                                <button class=\"btn btn-primary\">VOTE</button>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-lg-3 col-md-4 col-sm-6\">\n                <div class=\"card card-product\">\n                    <div class=\"card-image\" data-header-animation=\"true\">\n                        <a href=\"#pablo\">\n                            <img class=\"img\" src=\"../assets/img/card-2.jpeg\">\n                        </a>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"card-actions\">\n                            <button class=\"btn btn-primary btn-simple\">Read More</button>\n                        </div>\n                        <h4 class=\"card-title\">\n                            <a href=\"#pablo\">Cozy 5 Stars Apartment</a>\n                        </h4>\n                        <div class=\"card-description\">  <!--150 char-->\n                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt\n                            ut labore et dolore magna aliqua. Ut enim ad minim veniam.\n                        </div>\n                        <div class=\"row\">\n                            <div class=\"col-md-12 \">\n                                <div class=\"card-author text-left\">\n                                    <p class=\"card-author-p1\">Author</p>\n                                    <p class=\"\">Bruce Willis</p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"row\">\n                            <div class=\"col-md-2 col-sm-2 col-xs-3 img-votes\">\n                            </div>\n                            <div class=\"col-md-5 col-sm-5 col-xs-5 div-votes\">\n                                <h4 class=\"h4-votes-number\">98,231,1258\n                                    <span>VOTES</span>\n                                </h4>\n                            </div>\n                            <div class=\"col-md-5  col-sm-5 col-xs-4\">\n                                <button class=\"btn btn-primary\">VOTE</button>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-lg-3 col-md-4 col-sm-6\">\n                <div class=\"card card-product\">\n                    <div class=\"card-image\" data-header-animation=\"true\">\n                        <a href=\"#pablo\">\n                            <img class=\"img\" src=\"../assets/img/card-2.jpeg\">\n                        </a>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"card-actions\">\n                            <button class=\"btn btn-primary btn-simple\">Read More</button>\n                        </div>\n                        <h4 class=\"card-title\">\n                            <a href=\"#pablo\">Cozy 5 Stars Apartment</a>\n                        </h4>\n                        <div class=\"card-description\">  <!--150 char-->\n                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt\n                            ut labore et dolore magna aliqua. Ut enim ad minim veniam.\n                        </div>\n                        <div class=\"row\">\n                            <div class=\"col-md-12 \">\n                                <div class=\"card-author text-left\">\n                                    <p class=\"card-author-p1\">Author</p>\n                                    <p class=\"\">Bruce Willis</p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"row\">\n                            <div class=\"col-md-2 col-sm-2 col-xs-3 img-votes\">\n                            </div>\n                            <div class=\"col-md-5 col-sm-5 col-xs-5 div-votes\">\n                                <h4 class=\"h4-votes-number\">98,231,1258\n                                    <span>VOTES</span>\n                                </h4>\n                            </div>\n                            <div class=\"col-md-5  col-sm-5 col-xs-4\">\n                                <button class=\"btn btn-primary\">VOTE</button>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-lg-3 col-md-4 col-sm-6\">\n                <div class=\"card card-product\">\n                    <div class=\"card-image\" data-header-animation=\"true\">\n                        <a href=\"#pablo\">\n                            <img class=\"img\" src=\"../assets/img/card-2.jpeg\">\n                        </a>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"card-actions\">\n                            <button class=\"btn btn-primary btn-simple\">Read More</button>\n                        </div>\n                        <h4 class=\"card-title\">\n                            <a href=\"#pablo\">Cozy 5 Stars Apartment</a>\n                        </h4>\n                        <div class=\"card-description\">  <!--150 char-->\n                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt\n                            ut labore et dolore magna aliqua. Ut enim ad minim veniam.\n                        </div>\n                        <div class=\"row\">\n                            <div class=\"col-md-12 \">\n                                <div class=\"card-author text-left\">\n                                    <p class=\"card-author-p1\">Author</p>\n                                    <p class=\"\">Bruce Willis</p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"row\">\n                            <div class=\"col-md-2 col-sm-2 col-xs-3 img-votes\">\n                            </div>\n                            <div class=\"col-md-5 col-sm-5 col-xs-5 div-votes\">\n                                <h4 class=\"h4-votes-number\">98,231,1258\n                                    <span>VOTES</span>\n                                </h4>\n                            </div>\n                            <div class=\"col-md-5  col-sm-5 col-xs-4\">\n                                <button class=\"btn btn-primary\">VOTE</button>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-lg-3 col-md-4 col-sm-6\">\n                <div class=\"card card-product\">\n                    <div class=\"card-image\" data-header-animation=\"true\">\n                        <a href=\"#pablo\">\n                            <img class=\"img\" src=\"../assets/img/card-2.jpeg\">\n                        </a>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"card-actions\">\n                            <button class=\"btn btn-primary btn-simple\">Read More</button>\n                        </div>\n                        <h4 class=\"card-title\">\n                            <a href=\"#pablo\">Cozy 5 Stars Apartment</a>\n                        </h4>\n                        <div class=\"card-description\">  <!--150 char-->\n                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt\n                            ut labore et dolore magna aliqua. Ut enim ad minim veniam.\n                        </div>\n                        <div class=\"row\">\n                            <div class=\"col-md-12 \">\n                                <div class=\"card-author text-left\">\n                                    <p class=\"card-author-p1\">Author</p>\n                                    <p class=\"\">Bruce Willis</p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"row\">\n                            <div class=\"col-md-2 col-sm-2 col-xs-3 img-votes\">\n                            </div>\n                            <div class=\"col-md-5 col-sm-5 col-xs-5 div-votes\">\n                                <h4 class=\"h4-votes-number\">98,231,1258\n                                    <span>VOTES</span>\n                                </h4>\n                            </div>\n                            <div class=\"col-md-5  col-sm-5 col-xs-4\">\n                                <button class=\"btn btn-primary\">VOTE</button>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-lg-3 col-md-4 col-sm-6\">\n                <div class=\"card card-product\">\n                    <div class=\"card-image\" data-header-animation=\"true\">\n                        <a href=\"#pablo\">\n                            <img class=\"img\"\n                                 src=\"https://localhost:80/api/file/avatars/1504701726416_google_authenticator.png\">\n                        </a>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"card-actions\">\n                            <button class=\"btn btn-primary btn-simple\">Read More</button>\n                        </div>\n                        <h4 class=\"card-title\">\n                            <a href=\"#pablo\">Cozy 5 Stars Apartment</a>\n                        </h4>\n                        <div class=\"card-description\">  <!--150 char-->\n                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt\n                            ut labore et dolore magna aliqua. Ut enim ad minim veniam.\n                        </div>\n                        <div class=\"row\">\n                            <div class=\"col-md-12 \">\n                                <div class=\"card-author text-left\">\n                                    <p class=\"card-author-p1\">Author</p>\n                                    <p class=\"\">Bruce Willis</p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"row\">\n                            <div class=\"col-md-2 col-sm-2 col-xs-3 img-votes\">\n                            </div>\n                            <div class=\"col-md-5 col-sm-5 col-xs-5 div-votes\">\n                                <h4 class=\"h4-votes-number\">98,231,1258\n                                    <span>VOTES</span>\n                                </h4>\n                            </div>\n                            <div class=\"col-md-5  col-sm-5 col-xs-4\">\n                                <button class=\"btn btn-primary\">VOTE</button>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-lg-3 col-md-4 col-sm-6\">\n                <div class=\"card card-product\">\n                    <div class=\"card-image\" data-header-animation=\"true\">\n                        <a href=\"#pablo\">\n                            <img class=\"img\" src=\"../assets/img/card-2.jpeg\">\n                        </a>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"card-actions\">\n                            <button class=\"btn btn-primary btn-simple\">Read More</button>\n                        </div>\n                        <h4 class=\"card-title\">\n                            <a href=\"#pablo\">Cozy 5 Stars Apartment</a>\n                        </h4>\n                        <div class=\"card-description\">  <!--150 char-->\n                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt\n                            ut labore et dolore magna aliqua. Ut enim ad minim veniam.\n                        </div>\n                        <div class=\"row\">\n                            <div class=\"col-md-12 \">\n                                <div class=\"card-author text-left\">\n                                    <p class=\"card-author-p1\">Author</p>\n                                    <p class=\"\">Bruce Willis</p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-footer\">\n                        <div class=\"row\">\n                            <div class=\"col-md-2 col-sm-2 col-xs-3 img-votes\">\n                            </div>\n                            <div class=\"col-md-5 col-sm-5 col-xs-5 div-votes\">\n                                <h4 class=\"h4-votes-number\">98,231,1258\n                                    <span>VOTES</span>\n                                </h4>\n                            </div>\n                            <div class=\"col-md-5  col-sm-5 col-xs-4\">\n                                <button class=\"btn btn-primary\">VOTE</button>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\" blue-section\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-md-10 col-sm-12 col-xs-12\">\n                <h2>\n                    Became A Part Of <span>EINSTEINIUM</span> Community\n                </h2>\n            </div>\n            <div class=\"col-md-2 col-sm-2 col-xs-2 col-sm-10 col-xs-10\">\n                <button class=\"btn btn-round join-slack\">JOIN OUR SLACK</button>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\" gray-top-footer-section\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-md-12 col-sm-12 col-xs-12 div-text-on-screen\">\n                <h3 class=\"text-on-screen\">\n                    You may live in an unknown small village, but if you have big ideas, the world will come and find\n                    you!\n                </h3>\n            </div>\n            <div class=\"col-md-12 col-sm-12 col-xs-12 \">\n                <img src=\"../../../assets/imgEmc2/top-footer-screen.png\">\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\"to-top\" (click)=\"toTop()\">\n    <i style=\"font-size:24px; color: #fff;\" class=\"fa\">&#xf106;</i>\n</div>\n\n<app-big-footer></app-big-footer>"

/***/ }),

/***/ 724:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD0AAAAxCAYAAABpoKGSAAAACXBIWXMAAAsSAAALEgHS3X78AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAADS9JREFUeNrUWmlwHMUV7u6ZvXWs7tu6JVv4kA8MhhjZnI5SMcVhm3AEg53YlSoqhPDDoUglqfxJKj9SqSRA4YAD5KxKKgeEmBQmxDcWBmR84EOHZd0rabXa1R6zO915vX7rrKTZQ5bKOFPVe8z2zPTXr9/3vvd6KbnODiEETacfpVRc7TPodQSUQVPwPZ1xSdAcWgQmgP9fgQbAEqSld1zk/vETfYnLJ8oGPKR4zC9ydUFUEe1zua+JkUBpNu2ryqWumytZ//p6NgCnXdAmEXxa1lc/Z+uafntcLwLAK1w+8oWLbr42GCEF3qDI9YVIFheExUDLd4USbVIjrsEJ4ekaFYPekDi2cbGyF37qgDYK9wylY3X6OVrX8d1/RloOdvKfBcKkRgLj8CJw4caDnf4e+132r8im/T9/wLSroZAdhVPS8v5UwNm1ti40ubqyt7wW/uW75/ibQQA8l3teGhelLx3Wn4L3O+FrNbRMeIZyXSxvXM6WN9r0hfvO86dhkI/N1733d+grirOo8ux61cYoOQinzsHzPIl8XL1Wy/mbf4mU9U+IR8b9Yrs/TGpTXWMCW1kUGZoIAT8n4AIJD/Bz5c1T+hKHiTy67WaVWU0kCKc/g6Z9LqDRf23Ayg8B4K8DgGqK8cboKM6kpDaP8oZCqjnMNASnQt4QEb0e7jjRJ+y9HmHoknB/U+eoqO7z8Ftq81k3nOqBZ4eNrH0tLG0+epHXQQjaLgEn6uS0EbKuTuGNhdRfm0/HGwvZgEUlfTBBLk0nkR43W9DWwxtfb9Mrge1NRve46BYZZ4dFQ20+qYOvkti80PQ5g45TTDQF+wv8Pef7eyNvw5IuNZwRGMH6OiYeW6VoAHZM+iO089guQhs2KyRcl0/L6/KVZvi+4Y0P9aYe90zgJwe4+uElWtDapJRIQpPXzgl0nGqSD7PK8SZhfxFbwU/+IfzjQALA8th5iyLuXcwCdjOV4eYktP9AOy2JGZqcBD8qry5og1+oYfpHvSKrx60brhp4lhxbvowQcqxGS1xNxyfb+4Xte3v1xn4vWekNiBZORD3nxKlzYgcBoaBJI9A0k0rHbSbiyrGRfoWREPiZIUsXZhDytTWq1trEvHBdL5w6joCPooUk2Eic+0tyClY4KXeYydIMC6mYCM4cf4RHjSFB56Jh/LOyNFrX+spR/oRPExuAJZtBKZXFS8MpwkE2TUTNM+IjYcZmLi15wGSQrauV8Jea2CCy7EcIth1aPzQjAorIMCSXPIAdpJcnZMb4IWQxXNqZuCpn8KaaDPCrx3jux7380SEv+RaQULm0arruAP1N+NAZx8JCSkA+jiPgd6EdQyk5kgDwlWHBGDRfCNasbuxaFvUKLjUR76iJAO/9TDj3d/Dn/Bp5CACUCjE/VF6eTcmGRSwMFpE+/Am0D9CHJ1IAjpLnng/0EndAlGv6TCKzwJk8R1SChrFxo+ioGgHec4xnH+ziz7sD5Jn5AiuPHDslm5cz/b4lihu+nsJlLQlKqqdwGq5m/vtJ/X6I1S1cSNkydXCrK5hYW8Ok78v7exC4SKq9Y5nPoS6+zRskW9MKwrDgM4Au1DRU/G01VNxWw3zwsRPaEWTrUSSsVId0rSyfRtZqEWI36gCCJtJURF14/yEpbJIqMgn4SLew7WnTt3uC5GnwmVzDGxdQsrycRpaWUD/oXX+mhYbBjzgICAIpIXVNEto3LthZl7DAu+2zYWEfDxAKoUaAHwdg+cnYexhBy7AUTJUHo6rLvP9V7flhn1hrQF7knoWM37tYmbCa6Dl0G0mIWipFxt67wG+AMNQKtF9u9PAi4MOHV7DQklI6ZFXJBRQPchnp0tJ2E432WVxM6fJykXlpnJSdGaJN512i8KZKRiH5l2z9MbSDSFyTqdLAWKLy+O/COyBJ2WpETNV5VKo5rSaf9WEEOIkuo6fS3rTPI9YAcTUbdcyEkL+mkuk3LqD9cQOXBDRuoHpoYQa1QCwuWlmu3ASgV2bbSDa4Qg/81oas7Us0qOnu9sAe7dGBCfF0KEIcRv3uqFf4zZVsAia+A7miD/05ZWrJIP61wI2LjDre1cA4WNmLs7gPl6hcnoEE+YP0wQwpNOoLqARZgAqrHZP9SDqAN76ifbVnTLycaP2XZFHy1G1qAAD3IuALaGWeFHTsAeDHy4w6VedS0lhApO8OoKXakHV9GBYShmucFGlhO6Z6brSySAPw45A9PZeQ2aDXrx82BwGwHNen6Mu9+NyURQT5EBuEpzyjTpC1iNoCOolA29GXUy5PVFFeLNyxWAUz2XUxwF/erW0bmRTPSVGUqO9rj5jDFTm0D0PfAbS02yjJMAKtvN8hqoHEMg3jq43qTisdxQxI+s1EGoBj9WmeYjXMqB8A4J2DXvFDGE+WUQdwF7FluRJcUc4kv+yH9j6qOuluoVTRQI0F/cEJsUgQY5kJJBYGEnKhpUcTVSTmoeCgbvyV9g3IvXeB3DQEDCmm2Nys+O9sZF0I9E1cfYPphL94S5sgzpYk6mRRZE05ytIjSYhrroCVu14M7YCkZpeWgEzl8eAyxQuAL+TZ6WG08nGsfYdmU/eO+hCQgSOZb6Jf+tJUT7MuKd3+gvYkqMBfJBv1i5tM7lur2SmMHAeQvIbS0OyGoFm2NTGYkH4lPUtVLbmqKumdL2hbQmHy7WT9dm8xja5ewGL6QLYz6Grh2e5rXWHUcicdToTGEyAqxO8c+JiHVRM6X4BbX9ZawLV2gApsTNTvpc2mCQB8HPXBPrTwSCKZmY6l5UUaaOlOk0qC4E/W6Z2AWCyjk6KsNJtWYRnGM0tGTriHBYLoJwB4lVGfylzKwYc9oLYkUb0N7RBGDy+AvWo3YzhTQAJkCIL9pPEuAlG7xqJgpaVtc9kZwV0OGSUyf/BOpBVYelmCUrDYsJB5b6+PAn4H2nvQzqLamhOvsDiiGrWotDfB1gk9MyTsbn90iWdgwY1eBVgVJy3fHyZLTvSL74AgMqyurK9nwbsblQsgMw+ghbvTSVDSXd4EFYwPkoKDsNwW69PKQuDP5F9nuVNVyOKtN7JaLNwNAohgokHgpMQqqGq/RzjeOs3r2/vFF7MspKLMSYv7J8RKo2vzHZQ8u14dRFn5IS5pKS1pqn2quIOjODJOLeUPEoDTSk55rKTfHSAVBqVV+u45vuiBpWxdpiV6wxMycYDrJg38m2El0nKkmxe8dYq3nBjgO3whshD819pYCGm3RiyGCg7aT+8zBTA56cMszoSulfT40b5I0dFuvuiv28z/xiwriNu3+vRnxCxjAt+t/VO7vuuTfrEJGNtuVPUEZaaXOcklEyNnQCAchDtchBjvkbuscI3DNSlKvCGxACRkDbBytS8kFsFnc+xekG5GgInDYHVbBCtYsd/k1msOWPmeBhb2hogbCLQXDOAKhkUQfF9M36oV+CF2PRgmE/T6QpDNZ0Gm/nnnreohTECm8EB8askrnGRs1QL2vjvAl57RxDLNoOIIS13pGBFV8JCqXLtYoTDqBgL0S9AhXdjAgnnBMHECoKgl+bTFBaFRKcumis4ThUdBfv+RbpLzA/csTLU//b8JuPySa6d6YSbNOdDJy+9o4AX1BUyGuNO4i6nPAC0TibXV9NCwl1bBrBWfc4mSZMtpzE+K4GZFU+reKTRqpoVSIMyEv+tzoCnIuEjrIoWvqWKRD3p4DrjVxpY6okNiIpVkh3RFyUFqXDYk/VomEoPratlem8obe8fFJrkNOq8KLMoP8y7do8fKciYebFa0CicdBKCXvvK6tpQxcit8Po8cIcmQM4M00J/nIJ2tTezt3VvUv62ro975GlSW9fI2rN08//pdHsM+QSBblNs4HWaVtEN0sEGy5JRehTsezLDuLdc9bp98mmEm2c+0KP4MM197uJuXjkwax9RUR2EGJZuamVx2QfBpz+4jugX3mub1ONzF6ag/nLW+Tqn6zXF97c416qfb1yhdWAq+si9Gk0hEqcAWQJP/CamdDJFlfRNi6alBUTYeEFZY9mbIihRNF9FAKCuhdlN0l0FkWKgocJAwqKpgZS7R4LtOL+fgUi93do2JIJBho8NMioEYbaALWIwIxJRxzDw3tV/cK7xASFT+cVq3DUwIumWF2v3EakXW887jxqCsroxIo9Jk+S3GWgdapR7aDXICgORKIIyUjQdJTliXhQiQaRYiAAQxq5RDWNMgjI1g7XkMZzmABcEOTFMboFVh7Ww+/vAjo0rZa22R5lUVTO6VnUFhcziuyBBNUFJKyVjNCuVnLlY1y2Q+gILBNG3FCCSMIZSOsY1xKRa8uGel470y49LbuR7mth6+DFbR3ZubFQsWL/djuXlKRpb2w9DyMevLCXCijlYMCFrHgoMHrRr7SyOPK9qpJP2/RKYF2u0XtTYTvclqihrjGKrGsek596wfmOb/OEUcwIQaeLZJSxp7XXnIQ1as57mMcu45PTTZoOfyL905jMWMgKUx/ImKDNfFv4DnGXgMk7jWE39dH/8VYAAVu4DkcTvBvAAAAABJRU5ErkJggg=="

/***/ })

});
//# sourceMappingURL=1.chunk.js.map