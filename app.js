var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cors = require('cors');

var fs = require('fs');
var jwt = require('jsonwebtoken');

var settings = require('./controller/settings');
var cronJob = require('./controller/cronJobController');

var index = require('./routes/index');
var users = require('./routes/users');
var competitions = require('./routes/competitions');
var projects = require('./routes/projects');
var transaction = require('./routes/transaction');

var app = express();

mongoose.Promise = global.Promise;
mongoose.connect(settings.urlDB);

settings.transporter.verify(function (error, success) {
    if (error) {
        console.log(error);
    } else if (success) {
        console.log('Nodemailer is connected');
    }
});
setInterval(cronJob.createArrayAllDateEndCompetition, cronJob.getInterval(30));
var firstScanDb = setInterval(cronJob.createArrayAllDateEndCompetition, 2000);
clearInterval(firstScanDb);


setInterval(cronJob.compareCurrentDateAndDateEnd, cronJob.getInterval(1));

// app.use(function (req, res, next) {
//     res.header('Access-Control-Allow-Origin', "*");
//     res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
//     res.header('Access-Control-Allow-Headers', 'Content-Type');
//     next();
// });

/**
 * view engine setup
 */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

/**
 * Set Static Folder Angular
 */
app.use(express.static(path.join(__dirname, 'dist')));

/**
 * Set static public folder
 */
app.use('/api/file', express.static(path.join(__dirname, 'public/uploads')));

//corse
app.use(cors());

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var key = fs.readFileSync('./key/key.pem', 'utf8');
app.use(function (req, res, next) {
    if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'JWT') {
        jwt.verify(req.headers.authorization.split(' ')[1], key, function (err, decode) {
            if (err) req.user = undefined;
            req.user = decode;
            next();
        });
    } else {
        req.user = undefined;
        next();
    }
});

app.use('/', index);
app.use('/api/users', users);
app.use('/api/competition', competitions);
app.use('/api/projects', projects);
app.use('/api/transaction', transaction);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
