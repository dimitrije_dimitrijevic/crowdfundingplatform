/**
 * Created by Mita on 12.6.2017..
 */
var mongoose = require('mongoose'),
    jwt = require('jsonwebtoken'),
    nodemailer = require('nodemailer'),
    fs = require('fs'),
    settings = require('./settings'),
    User = require("./models/user"),
    bcrypt = require("bcrypt"),
    crypto = require('crypto'),
    multer = require('multer'),
    mkdirp = require('mkdirp'),
    path = require('path');

var key = fs.readFileSync('./key/key.pem', 'utf8');

exports.login = function (req, res) {
    var username = req.body.username;
    var password = req.body.password;
    User.findOne({username: username}, function (err, user) {
        if (err) {
            return res.status(settings.statusCode_Service_Unavailable_ErrorDb).json({
                error: true,
                message: 'DB_error',
                statusCode: settings.statusCode_Service_Unavailable_ErrorDb
            });
        }
        if (!user) {
            return res.status(settings.statusCode_Bad_Request).json({
                error: true,
                message: 'user_no_exists',
                statusCode: settings.statusCode_Bad_Request
            });
        } else {
            user.checkPassword(password, function (err, isMatch) {
                if (err) {
                    return res.status(settings.statusCode_Bad_Request).json({
                        error: true,
                        message: 'error_match_Password',
                        statusCode: settings.statusCode_Bad_Request
                    });
                }
                if (isMatch) {
                    if (user.checkConfirmEmail()) {
                        if (!user.two_factor_enabled) {
                            user.createTwoFactorSecretKey(function (img_url, secureCode) {
                                // return res.status(settings.statusCode_OK).send("<img src=" + img_url + ">");
                                return res.status(settings.statusCode_OK).json({
                                    error: false,
                                    imageSrc: img_url,
                                    secureCode: secureCode,
                                    message: 'new_qrCode',
                                    statusCode: settings.statusCode_OK
                                });
                            });
                        } else {
                            return res.status(settings.statusCode_OK).json({
                                error: false,
                                twoFactor: 'enable',
                                statusCode: settings.statusCode_OK
                            });
                        }
                    } else {
                        return res.status(settings.statusCode_OK).json({
                            error: true,
                            message: 'check_email_verify_account'
                        });
                    }
                } else {
                    return res.status(settings.statusCode_Bad_Request).json({
                        error: true,
                        message: 'invalid_password',
                        statusCode: settings.statusCode_Bad_Request
                    });
                }
            });
        }
    });
};
exports.twoFactorVerify = function (req, res) {
    User.findOne({username: req.body.username}, function (err, user) {
        if (err) {
            return res.status(settings.statusCode_Service_Unavailable_ErrorDb).json({
                error: true,
                message: 'DB_error',
                statusCode: settings.statusCode_Service_Unavailable_ErrorDb
            });
        }
        if (!user) {
            return res.status(settings.statusCode_Bad_Request).json({
                error: true,
                message: 'user_no_exists',
                statusCode: settings.statusCode_Bad_Request
            });
        } else {
            if (user.two_factor_enabled) {
                user.verifyTwoFactor(req.body.token2fa, function (success) {
                    if (success) {
                        return res.status(settings.statusCode_OK).json({
                            error: false,
                            statusCode: settings.statusCode_OK,
                            username: user.username,
                            tokenJWT: jwt.sign({
                                username: user.username,
                                _id: user._id,
                                permission: user.permission
                            }, key, {
                                algorithm: 'HS256',
                                expiresIn: '10h'
                            })
                        });
                    } else {
                        return res.status(settings.statusCode_Bad_Request).json({
                            error: true,
                            statusCode: settings.statusCode_Bad_Request,
                            message: "token_not_valid"
                        });
                    }
                })
            }
        }
    });
};
exports.twoFactorVerifyResetCode = function (req, res) {
    User.findOne({username: req.body.username}, function (err, user) {
        if (err) {
            return res.status(settings.statusCode_Service_Unavailable_ErrorDb).json({
                error: true,
                message: 'DB_error',
                statusCode: settings.statusCode_Service_Unavailable_ErrorDb
            });
        }
        if (!user) {
            return res.status(settings.statusCode_Bad_Request).json({
                error: true,
                message: "user_no_exists",
                statusCode: settings.statusCode_Bad_Request
            });
        } else {
            if (user.two_factor_enabled) {
                user.two_factor_enabled = false;
                user.save(function () {
                    return res.status(settings.statusCode_OK).json({
                        error: false,
                        message: "success_qr_reset_please_login_again",
                        statusCode: settings.statusCode_OK
                    });
                });
            }
        }
    });
};
exports.twoFactorVerifyDeliverQrCodeSave = function (req, res) {
    User.findOne({username: req.body.username}, function (err, user) {
        if (err) {
            return res.status(settings.statusCode_Service_Unavailable_ErrorDb).json({
                error: true,
                message: 'DB_error',
                statusCode: settings.statusCode_Service_Unavailable_ErrorDb
            });
        }
        if (!user) {
            return res.status(settings.statusCode_Bad_Request).json({
                error: true,
                message: 'user_no_exists',
                statusCode: settings.statusCode_Bad_Request
            });
        } else {
            if (!user.two_factor_enabled) {
                user.two_factor_enabled = true;
                user.save(function () {
                    return res.status(settings.statusCode_OK).json({
                        error: false,
                        statusCode: settings.statusCode_OK,
                        message: 'qr_code_save'
                    });
                });
            }
        }
    });
};


exports.register = function (req, res) {
    // console.log(req.body);
    var username = req.body.username;
    var password = req.body.password;
    User.findOne({username: username}, function (err, user) {
        if (err) {
            return res.status(settings.statusCode_Service_Unavailable_ErrorDb).json({
                error: true,
                message: 'DB_error',
                statusCode: settings.statusCode_Service_Unavailable_ErrorDb
            });
        }
        if (user) {
            return res.status(settings.statusCode_Bad_Request).json({
                error: true,
                message: 'user_exists',
                statusCode: settings.statusCode_Bad_Request
            });
        }
        var newUser = new User({
            username: username,
            password: password,
            givenName: req.body.givenName,
            familyName: req.body.familyName,
            tel: req.body.phone,
            city: req.body.city,
            state: req.body.state,
            birthday: req.body.birthday
        });
        newUser.save(function () {
            res.status(settings.statusCode_OK).json({
                error: false,
                message: "success_user_created",
                statusCode: settings.statusCode_OK
            });
        });
    });
};
exports.registerAvatarForUser = function (req, res) {
    var newPath = path.join(__dirname, '../public/uploads/avatars/');
    var nameImage;
    mkdirp(newPath, function (err) {
        if (err) console.error(err);
        else {
            var storage = multer.diskStorage({
                destination: function (req, file, callback) {
                    callback(null, newPath);
                },
                filename: function (req, file, callback) {
                    nameImage = Date.now() + '_' + file.originalname;
                    // nameImage = Date.now() + '_' + req.body.id + file.originalname;
                    callback(null, nameImage);
                }
            });
            var upload = multer({storage: storage}).single('avatar');
            upload(req, res, function (err) {
                if (err) {
                    return res.end("Error uploading file.");
                }
                // console.log(req);
                var username = req.body.username;
                User.findOne({username: username}, function (err, user) {
                    if (err) {
                        return res.status(settings.statusCode_Service_Unavailable_ErrorDb).json({
                            error: true,
                            message: 'DB_error',
                            statusCode: settings.statusCode_Service_Unavailable_ErrorDb
                        });
                    }
                    if (!user) {
                        return res.status(settings.statusCode_Bad_Request).json({
                            error: true,
                            message: 'user_no_exists',
                            statusCode: settings.statusCode_Bad_Request
                        });
                    }
                    if (user) {
                        user.avatarImage = newPath + nameImage;
                        user.save(function () {
                            res.status(settings.statusCode_OK).json({
                                error: false,
                                message: 'user_upload_avatar',
                                statusCode: settings.statusCode_OK
                            });
                        });
                    }
                });

            });
        }
    });
};
exports.verifyAccountFromMail = function (req, res) {
    // console.log(req.body);
    User.findOne({
        username: req.body.username,
        verifyAccountToken: req.body.tokenVerify,
        verifyAccountExpires: {$gt: Date.now()}
    }, function (err, user) {
        if (err) {
            return res.status(settings.statusCode_Service_Unavailable_ErrorDb).json({
                error: true,
                message: 'DB_error',
                statusCode: settings.statusCode_Service_Unavailable_ErrorDb
            });
        }
        if (!user) {
            return res.status(settings.statusCode_Bad_Request).json({
                error: true,
                message: "error_verification_or_token_expires",
                statusCode: settings.statusCode_Bad_Request
            });
        }
        if (user) {
            user.verifyAccount = true;
            user.verifyAccountToken = '';
            user.verifyAccountExpires = '';
            user.save();
            res.status(settings.statusCode_OK).json({
                error: false,
                message: "success_user_verify",
                statusCode: settings.statusCode_OK
            });
        }
    });
};
exports.sendNewTokenVerifyAccount = function (req, res) {
    var username = req.body.username;
    User.findOne({username: username}, function (err, user) {
        if (err) {
            return res.status(settings.statusCode_Service_Unavailable_ErrorDb).json({
                error: true,
                message: 'DB_error',
                statusCode: settings.statusCode_Service_Unavailable_ErrorDb
            });
        }
        if (user) {
            if (!user.checkConfirmEmail()) {
                user.createVerifyTokenAccount(function () {
                    user.save(function () {
                        return res.status(settings.statusCode_OK).json({
                            error: false,
                            message: "success_send_mail_for_verify",
                            statusCode: settings.statusCode_OK
                        });
                    });
                });
            } else {
                return res.status(settings.statusCode_Bad_Request).json({
                    error: true,
                    message: "account_is_verify",
                    statusCode: settings.statusCode_Bad_Request
                });
            }
        }
    });
};


exports.resetPasswordSendMail = function (req, res) {
    User.findOne({username: req.body.username}, function (err, user) {
        if (err) {
            return res.status(settings.statusCode_Service_Unavailable_ErrorDb).json({
                error: true,
                message: 'DB_error',
                statusCode: settings.statusCode_Service_Unavailable_ErrorDb
            });
        }
        if (!user) {
            return res.status(settings.statusCode_Bad_Request).json({
                error: true,
                message: 'user_no_exists',
                statusCode: settings.statusCode_Bad_Request
            });
        }
        if (user) {
            crypto.randomBytes(50, function (err, buf) {
                user.resetPasswordToken = buf.toString('hex');
                user.resetPasswordExpires = Date.now() + 3600000;  //1h
                user.two_factor_enabled = false;
                user.save();

                var address = settings.urlAddressFrontendResetPassword + "/" + user.username + "/" + user.resetPasswordToken;
                var mailOptions = {
                    from: settings.transporterMailName + '<' + settings.transporterMail + '>',
                    to: user.username,
                    subject: 'Reset password',
                    text: 'Reset password',
                    html: '<a href=\"' + address + '\">' + address + '</a>' // html body
                };
                settings.transporter.sendMail(mailOptions, function (error, info) {
                    if (error) {
                        return console.log(error);
                    }
                    res.status(settings.statusCode_OK).json({
                        error: false,
                        token_expired: '1h',
                        message: "mail_send_from_token_for_new_password",
                        statusCode: settings.statusCode_OK
                    })
                    ;
                    console.log('Message %s sent: %s', info.messageId, info.response);
                });
            });
        }
    });
};
exports.resetPasswordSetNew = function (req, res) {
    User.findOne(
        {
            username: req.body.username,
            resetPasswordToken: req.body.token,
            resetPasswordExpires: {$gt: Date.now()}
        },
        function (err, user) {
            if (err) {
                return res.status(settings.statusCode_Service_Unavailable_ErrorDb).json({
                    error: true,
                    message: 'DB_error',
                    statusCode: settings.statusCode_Service_Unavailable_ErrorDb
                });
            }
            if (!user) {
                return res.status(settings.statusCode_Bad_Request).json({
                    error: true,
                    message: "no_set_new_pass_or_token_expired",
                    statusCode: settings.statusCode_Bad_Request
                });
            }
            if (user) {
                user.newPassword(req.body.password, function () {
                    user.save(function () {
                        return res.status(settings.statusCode_OK).json({
                            error: false,
                            message: "password_changed",
                            statusCode: settings.statusCode_OK
                        });
                    });
                });
            }
        });
};


// exports.testToken = function (req, res) {
//     var user = req.user;
//     console.log(user);
//     if (req.user) {
//         jwt.verify(req.body.token, key, function (err, decode) {
//             if (err) {
//                 // console.log(err);
//                 res.send(err);
//             } else {
//                 // console.log(decode);
//                 res.send(decode);
//             }
//         });
//     } else {
//         res.send("please login");
//     }
// };

Date.prototype.addHours = function (h) {
    var copiedDate = new Date();
    copiedDate.setTime(this.getTime() + (h * 60 * 60 * 1000));
    return copiedDate;
};