/**
 * Created by Mita on 21.6.2017..
 */
var mongoose = require('mongoose'),
    settings = require('./settings'),
    User = require('./models/user'),
    Project = require('./models/project'),
    Transaction = require('./models/transaction'),
    fs = require('fs');


exports.walletCallbackInfo = function (req, res) {
    var invoice_id = req.body.invoide_id;
    var status = req.body.status;
    var data = JSON.stringify(req.body.data);
    var signature = req.body.signature;

    switch (status) {
        case 'new':
            Transaction.findOne({walletInvoice_id: invoice_id}, function (err, transaction) {
                if (err) {
                    return res.status(200).json({
                        "error": "DB_error"
                    });
                }
                if (!transaction) {
                    return res.status(200).json({
                        "error": "transaction_no_exists"
                    });
                } else {
                    transaction.setCallBackInfoStatus(status, function () {


                    });
                }
            });
            break;
        case 'paid':
            Transaction.findOne({walletInvoice_id: invoice_id}, function (err, transaction) {
                if (err) {
                    return res.status(200).json({
                        "error": "DB_error"
                    });
                }
                if (!transaction) {
                    return res.status(200).json({
                        "error": "transaction_no_exists"
                    });
                } else {
                    transaction.setCallBackInfoStatus(status, function () {


                    });
                }
            });
            break;
        case 'confirmed':
            Transaction.findOne({walletInvoice_id: invoice_id}, function (err, transaction) {
                if (err) {
                    return res.status(200).json({
                        "error": "DB_error"
                    });
                }
                if (!transaction) {
                    return res.status(200).json({
                        "error": "transaction_no_exists"
                    });
                } else {
                    transaction.setCallBackInfoStatus(status, function () {

                        Project.findById(data.project_id, function (err, project) {
                            if (err) {
                                return res.status(200).json({
                                    "error": "DB_error"
                                });
                            }
                            if (!project) {
                                return res.status(200).json({
                                    "error": "project_no_exists"
                                });
                            } else {
                                project.setStatus(true, data.username);
                            }
                        });

                    });
                }
            });
            break;
        case 'failed':
            Transaction.findOne({walletInvoice_id: invoice_id}, function (err, transaction) {
                if (err) {
                    return res.status(200).json({
                        "error": "DB_error"
                    });
                }
                if (!transaction) {
                    return res.status(200).json({
                        "error": "transaction_no_exists"
                    });
                } else {
                    transaction.setCallBackInfoStatus(status, function () {


                    });
                }
            });
            break;
        case 'expired':
            Transaction.findByIdAndRemove({walletInvoice_id: invoice_id}, function (err, transaction) {
                if (err) {
                    return res.status(200).json({
                        "error": "DB_error"
                    });
                }
                if (!transaction) {
                    return res.status(200).json({
                        "error": "transaction_no_exists"
                    });
                } else {

                    Project.findByIdAndRemove(data.project_id, function (err, project) {
                        if (err) {
                            return res.status(200).json({
                                "error": "DB_error"
                            });
                        }
                        if (!project) {
                            return res.status(200).json({
                                "error": "project_no_exists"
                            });
                        } else {

                            var mailOptions = {
                                from: settings.transporterMailName + '<' + settings.transporterMail + '>',
                                to: data.username,
                                subject: 'Info Project Status',
                                text: 'Paid expired, your project is removed!'
                                // html: '<a href=\"' + address + '\">' + address + '</a>' // html body
                            };
                            settings.transporter.sendMail(mailOptions, function (error, info) {
                                if (error) {
                                    return console.log(error);
                                }
                                console.log('Message %s sent: %s', info.messageId, info.response);
                            });

                        }
                    });

                }
            });
            break;
    }

};

exports.getAllCurrentTransaction = function (req, res) {
    if (req.user && req.user.permission === 'admin') {
        var data = [];

        var cursor = Transaction.find().cursor();
        cursor.on('data', function (doc) {

            data.push({
                walletInvoice_id: doc.walletInvoice_id,
                walletStatus: doc.walletStatus,
                walletDetailTransaction: doc.walletDetailTransaction,
                walletData: doc.walletData,
                walletPrice: doc.walletPrice,
                createdAt: doc.createdAt
            });

        });
        cursor.on('close', function () {
            res.status(200).json(data);
        });

    } else {
        res.status(200).json({
            "error": "user_not_valid"
        });
    }
};


