/**
 * Created by Mita on 12.6.2017..
 */
var express = require('express');
var nodemailer = require('nodemailer');

module.exports = {
    urlHostServer:'https://localhost:80',
    urlDB: 'mongodb://localhost:27017/Test',
    transporterMailName: 'Dimitrije Dimitrijevic',
    transporterMail: 'dimitrije.dimitrijevic@itcentar.rs',
    transporter: nodemailer.createTransport({
        host: 'host25.dwhost.net',
        port: 465,
        secure: true, // secure:true for port 465, secure:false for port 587
        auth: {
            user: 'dimitrije.dimitrijevic@itcentar.rs',
            pass: 'RWfWcgh!i9F('
        }
    }),
    urlAddressFrontendResetPassword: ' http://localhost:4200/pages/changePassword',
    urlAddressFrontendVerifyAccountWithMail: 'http://localhost:4200/pages/userActivation',
    walletAddressGetNewAddress: 'http://wallet.emc2.foundation/newaddress',
    walletAddressGetCreateInvoice: 'http://wallet.emc2.foundation/createinvoice',
    walletAddressGetInvoice: 'http://wallet.emc2.foundation/getinvoice',
    walletAppID: '',
    walletApiAuthToken: '',
    walletApiAuthAddress: '',
    defaultPriceForAddNewProject: 1000,
    statusCode_Service_Unavailable_ErrorDb: 503,
    statusCode_Not_Found_Resource: 404,
    statusCode_Bad_Request: 400,
    statusCode_OK: 200,
    statusCode_Non_authoritative_Information: 203

};