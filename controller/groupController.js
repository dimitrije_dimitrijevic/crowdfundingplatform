/**
 * Created by Mita on 13.6.2017..
 */

var mongoose = require('mongoose'),
    settings = require('./settings'),
    assert = require('assert'),
    Group = require("./models/group");

exports.addNewGroup = function (req, res) {
    if (req.user && req.user.permission === 'admin') {
        var name = req.body.name;
        var description = req.body.description;
        Group.findOne({name: name}, function (err, group) {
            if (err) {
                return res.status(settings.statusCode_Service_Unavailable_ErrorDb).json({
                    error: true,
                    message: 'DB_error',
                    statusCode: settings.statusCode_Service_Unavailable_ErrorDb
                });
            }
            if (group) {
                return res.status(settings.statusCode_Bad_Request).json({
                    error: true,
                    message: 'tag_exists',
                    statusCode: settings.statusCode_Bad_Request
                });
            }
            var newGroup = new Group({
                name: name,
                description: description
            });

            newGroup.save(function () {
                return res.status(settings.statusCode_OK).json({
                    error: false,
                    message: 'group_created'
                });
            });
        });
    } else {
        res.status(settings.statusCode_Bad_Request).json({
            error: true,
            message: "user_not_valid_permission"
        });
    }
};

exports.getAllGroup = function (req, res) {
    // console.log(req);
    if (req.user) {
        var cursor = Group.find().cursor();
        var data = [];
        cursor.on('data', function (doc) {
            data.push({
                name: doc.name
            });
        });
        cursor.on('close', function () {
            // console.log(data);
            res.status(settings.statusCode_OK).json(data);
            // res.end();
        });
    } else {
        res.status(settings.statusCode_Bad_Request).json({
            error: true,
            message: "user_not_valid"
        });
    }
};

