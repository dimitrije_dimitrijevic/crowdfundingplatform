/**
 * Created by Mita on 22.6.2017..
 */
var express = require('express');
var mongoose = require('mongoose'),
    settings = require('./settings'),
    dateEndArray = [],
    Competition = require('./models/competition');


module.exports.createArrayAllDateEndCompetition = function () {
    dateEndArray = [];
    var cursor = Competition.find({status: 'active'}).cursor();
    cursor.on('data', function (doc) {
        dateEndArray.push({
            competitionId: doc.id,
            dateEnd: doc.dateEnd
        });
    });

    cursor.on('close', function () {
        // console.log('compairCurrentDateAndDateEnd');
        // console.log(dateEndArray);
    });
};

module.exports.compareCurrentDateAndDateEnd = function () {
    dateEndArray.forEach(function (filename, count, array) {

        if (new Date() >= filename.dateEnd) {
            Competition.findById({_id: filename.competitionId}, function (err, competition) {
                if (err) {
                    return res.status(200).json({
                        "error": "DB_error"
                    });
                }
                if (!competition) {
                    return res.status(200).json({
                        "error": "competition_no_exists"
                    });
                } else {
                    competition.setStatus('finished', function () {
                        competition.save(function () {
                            // console.log('compairCurrentDateAndDateEnd');
                        });
                    });
                }
            });
        } else {
            // console.log('Konkursi i dalje traju');
        }

    });
};

module.exports.getInterval = function (minutes) {
    return minutes * 60 * 1000;
};

