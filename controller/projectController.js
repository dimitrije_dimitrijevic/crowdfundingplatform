/**
 * Created by Mita on 19.6.2017..
 */
var mongoose = require('mongoose'),
    settings = require('./settings'),
    assert = require('assert'),
    User = require('./models/user'),
    Project = require('./models/project'),
    Transaction = require('./models/transaction'),
    Competition = require('./models/competition'),
    fs = require('fs'),
    multer = require('multer'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    request = require('request');

/**
 * CREATE NEW PROJECT
 */
exports.addNewProject = function (req, res) {
    if (req.user) {
        Competition.findOne({
            _id: req.body.competitionId,
            // status: 'action',
            dataStartRevision: {$gt: Date.now()}
        }, function (err, competition) {
            if (err) {
                return res.status(settings.statusCode_Service_Unavailable_ErrorDb).json({
                    error: true,
                    message: 'DB_error',
                    statusCode: settings.statusCode_Service_Unavailable_ErrorDb
                });
            }
            if (!competition) {
                return res.status(settings.statusCode_Bad_Request).json({
                    error: true,
                    message: 'competition_no_exists_or_revision_time_start',
                    statusCode: settings.statusCode_Bad_Request
                });
            } else {
                /**
                 * Func Dodaj Project
                 * Ukoliko postoji konkurs, i ukoiko nije proslo vreme za reviziju, moguce je dodati projekat
                 */

                var documentationName;
                var allImage = [];
                var albumImageDirectoryName = req.body.competitionId + '/' + Date.now() + '-' + req.user._id;
                var newPath = path.join(__dirname, '../public/uploads/projects/') + albumImageDirectoryName;
                mkdirp(newPath, function (err) {
                    if (err) console.error(err);
                    else {
                        var storage = multer.diskStorage({
                            destination: function (req, file, callback) {
                                callback(null, newPath);
                            },
                            filename: function (req, file, callback) {
                                if (file.fieldname === 'gallery') {
                                    var nameImage = Date.now() + '-' + file.originalname;
                                    allImage.push(nameImage);
                                    callback(null, nameImage);
                                } else if (file.fieldname === 'documentation') {
                                    documentationName = file.originalname;
                                    callback(null, documentationName);
                                }
                            }
                        });
                        // var upload = multer({storage: storage}).array('upload_file');
                        var upload = multer({storage: storage}).fields([
                            {
                                name: 'documentation',
                                maxCount: 1
                            }, {
                                name: 'gallery',
                                maxCount: 10
                            }
                        ]);
                        upload(req, res, function (err) {
                            if (err) {
                                return res.status(settings.statusCode_Bad_Request).json({
                                    error: true,
                                    message: 'error_create_project',
                                    statusCode: settings.statusCode_Bad_Request
                                    // Error uploading file.
                                });
                            }
                            var newProject = new Project({
                                userId: req.user._id,
                                competitionId: req.body.competitionId,
                                groupId: req.body.groupId,
                                name: req.body.name,
                                descriptionFull: req.body.descriptionFull,
                                descriptionShort: req.body.descriptionShort,
                                youtubeLink: req.body.youtubeLink,
                                albumImage: allImage,
                                directoryNameProject: albumImageDirectoryName,
                                documentation: documentationName
                            });
                            newProject.save(function () {
                                // sendCreateInvoiceWaitResponseAndCreateTransaction(req.user, newProject, function () {
                                // TODO return user invoice for add new project
                                return res.status(settings.statusCode_OK).json({
                                    error: false,
                                    message: 'new_project_created',
                                    statusCode: settings.statusCode_OK
                                    // });
                                });
                            });
                        });
                    }
                });

                /**
                 * END Dodaj Project
                 */
            }
        });
    } else {
        res.status(settings.statusCode_Bad_Request).json({
            error: true,
            message: 'user_not_valid',
            statusCode: settings.statusCode_Bad_Request
        });
    }
};
exports.editProject = function (req, res) {
    // if (req.user) {
    //     Competition.findOne({
    //         _id: req.body.competitionId,
    //         status: 'action',
    //         dataStartRevision: {$gt: Date.now()}
    //     }, function (err, competition) {
    //         if (err) {
    //             return res.status(settings.statusCode_Service_Unavailable_ErrorDb).json({
    //                 error: true,
    //                 message: 'DB_error',
    //                 statusCode: settings.statusCode_Service_Unavailable_ErrorDb
    //             });
    //         }
    //         if (!competition) {
    //             return res.status(settings.statusCode_Bad_Request).json({
    //                 error: true,
    //                 message: 'competition_no_exists_or_revision_time_start',
    //                 statusCode: settings.statusCode_Bad_Request
    //             });
    //         } else {
    //             /**
    //              * Func Dodaj Project
    //              * Ukoliko postoji konkurs, i ukoiko nije proslo vreme za reviziju, moguce je dodati projekat
    //              */
    //             var documentationName;
    //             var allImage = [];
    //             var albumImageDirectoryName = Date.now() + '-' + req.user._id;
    //             var newPath = path.join(__dirname, '../public/uploads/projects/') + req.body.competitionId + '/' + albumImageDirectoryName;
    //             mkdirp(newPath, function (err) {
    //                 if (err) console.error(err);
    //                 else {
    //                     var storage = multer.diskStorage({
    //                         destination: function (req, file, callback) {
    //                             callback(null, newPath);
    //                         },
    //                         filename: function (req, file, callback) {
    //                             if (file.fieldname === 'gallery') {
    //                                 var nameImage = Date.now() + '-' + file.originalname;
    //                                 allImage.push(nameImage);
    //                                 callback(null, nameImage);
    //                             } else if (file.fieldname === 'documentation') {
    //                                 documentationName = file.originalname;
    //                                 callback(null, documentationName);
    //                             }
    //                         }
    //                     });
    //                     // var upload = multer({storage: storage}).array('upload_file');
    //                     var upload = multer({storage: storage}).fields([
    //                         {
    //                             name: 'documentation',
    //                             maxCount: 1
    //                         }, {
    //                             name: 'gallery',
    //                             maxCount: 10
    //                         }
    //                     ]);
    //                     upload(req, res, function (err) {
    //                         if (err) {
    //                             return res.status(settings.statusCode_Bad_Request).json({
    //                                 error: true,
    //                                 message: 'error_create_project',
    //                                 statusCode: settings.statusCode_Bad_Request
    //                                 // Error uploading file.
    //                             });
    //                         }
    //                         var newProject = new Project({
    //                             userId: req.user._id,
    //                             competitionId: req.body.competitionId,
    //                             groupId: req.body.groupId,
    //                             name: req.body.name,
    //                             descriptionFull: req.body.descriptionFull,
    //                             descriptionShort: req.body.descriptionShort,
    //                             youtubeLink: req.body.youtubeLink,
    //                             albumImage: allImage,
    //                             directoryNameProject: albumImageDirectoryName,
    //                             documentation: documentationName
    //                         });
    //                         newProject.save(function () {
    //                             // sendCreateInvoiceWaitResponseAndCreateTransaction(req.user, newProject, function () {
    //                             // TODO return user invoice for add new project
    //                             return res.status(settings.statusCode_OK).json({
    //                                 error: false,
    //                                 message: 'new_project_created',
    //                                 statusCode: settings.statusCode_OK
    //                                 // });
    //                             });
    //                         });
    //                     });
    //                 }
    //             });
    //
    //             /**
    //              * END Dodaj Project
    //              */
    //         }
    //     });
    // } else {
    //     res.status(settings.statusCode_Bad_Request).json({
    //         error: true,
    //         message: 'user_not_valid',
    //         statusCode: settings.statusCode_Bad_Request
    //     });
    // }
};
/**
 * END CREATE NEW PROJECT
 */


exports.getCurrentProject = function (req, res) {
    if (req.user) {
        Project.findOne({userId: req.user._id, _id: req.body.projectId}, function (err, project) {
            if (err) {
                return res.status(settings.statusCode_Service_Unavailable_ErrorDb).json({
                    error: true,
                    message: 'DB_error',
                    statusCode: settings.statusCode_Service_Unavailable_ErrorDb
                });
            }
            if (!project) {
                return res.status(settings.statusCode_Bad_Request).json({
                    error: true,
                    message: 'project_no_exists',
                    statusCode: settings.statusCode_Bad_Request
                });
            }
            var data = [];
            var images = [];
            project.albumImage.forEach(function (filename, count, array) {
                images.push(settings.urlHostServer + '/api/file/projects/' + project.directoryNameProject + '/' + filename)
            });
            project.push({
                name: project.name,
                descriptionFull: project.descriptionFull,
                descriptionShort: project.descriptionShort,
                youtubeLink: project.youtubeLink,
                documentation: settings.urlHostServer + '/api/file/projects/' + project.directoryNameProject + '/' + project.documentation,
                albumImage: images
            });
            res.status(settings.statusCode_OK).json({
                error: false,
                message: data,
                statusCode: settings.statusCode_OK
            });
        });
    } else {
        res.status(settings.statusCode_Bad_Request).json({
            error: true,
            message: 'user_not_valid',
            statusCode: settings.statusCode_Bad_Request
        });
    }
};
exports.getAllActiveProject = function (req, res) {
    if (req.user) {
        var cursor = Project.find({statusPaid: true}).cursor();
        var data = [];
        cursor.on('data', function (doc) {
            var images = [];
            doc.albumImage.forEach(function (filename, count, array) {
                images.push(settings.urlHostServer + '/api/file/projects/' + doc.directoryNameProject + '/' + filename)
            });

            data.push({
                name: doc.name,
                descriptionFull: doc.descriptionFull,
                descriptionShort: doc.descriptionShort,
                youtubeLink: doc.youtubeLink,
                documentation: settings.urlHostServer + '/api/file/projects/' + doc.directoryNameProject + '/' + doc.documentation,
                albumImage: images
            });
        });
        cursor.on('close', function () {
            res.status(settings.statusCode_OK).json({
                error: false,
                message: data,
                statusCode: settings.statusCode_OK
            });
        });

    } else {
        res.status(settings.statusCode_Bad_Request).json({
            error: true,
            message: 'user_not_valid',
            statusCode: settings.statusCode_Bad_Request
        });
    }
};
exports.getAllUserProject = function (req, res) {
    if (req.user) {
        var cursor = Project.find({userId: req.user._id, status: true}).cursor();
        var data = [];
        cursor.on('data', function (doc) {
            var images = [];
            doc.albumImage.forEach(function (filename, count, array) {
                images.push(settings.urlHostServer + '/api/file/projects/' + doc.directoryNameProject + '/' + filename)
            });

            data.push({
                name: doc.name,
                descriptionFull: doc.descriptionFull,
                descriptionShort: doc.descriptionShort,
                youtubeLink: doc.youtubeLink,
                documentation: settings.urlHostServer + '/api/file/projects/' + doc.directoryNameProject + '/' + doc.documentation,
                albumImage: images
            });
        });
        cursor.on('close', function () {
            res.status(200).json({
                error: false,
                message: data,
                statusCode: settings.statusCode_OK
            });
        });

    } else {
        res.status(settings.statusCode_Bad_Request).json({
            error: true,
            message: 'user_not_valid',
            statusCode: settings.statusCode_Bad_Request
        });
    }
};


// exports.getProjectImage = function (req, res) {
//     if (req.user) {
//         var image = path.join(__dirname, '../public/uploads/projects/')
//             + req.params.fileName.split(':')[1]
//             + '/' + req.params.imageName.split(':')[1];
//         res.sendFile(image);
//     } else {
//         res.status(settings.statusCode_Bad_Request).json({
//             error: true,
//             message: 'user_not_valid',
//             statusCode: settings.statusCode_Bad_Request
//         });
//     }
// };

// exports.uploadSingleFile = function (req, res) {
//     var newPath = './public/uploads/' + 'mita';
//     fs.mkdirSync(newPath);
//
//     var storage = multer.diskStorage({
//         destination: function (req, file, callback) {
//             callback(null, newPath);
//         },
//         filename: function (req, file, callback) {
//             callback(null, Date.now() + file.originalname);
//         }
//     });
//     var upload = multer({storage: storage}).array('upload_file');
//     upload(req, res, function (err) {
//         if (err) {
//             return res.end("Error uploading file.");
//         }
//         res.end("File is uploaded :" + req.body.name);
//     });
// };

var sendCreateInvoiceWaitResponseAndCreateTransaction = function (user, project, callback) {

    var headers = {
        'Content-Type': 'application/json'
    };

    var walletData = {
        token: settings.walletApiAuthToken,
        name: 'Add New Project id:' + project._id,
        description: 'Add New Project, name: ' + project.name,
        price: settings.defaultPriceForAddNewProject,
        currency: 'emc2',
        address: settings.walletApiAuthAddress,
        data: '{username:' + user.username + ', date: ' + project.createdAt + ', projectId: ' + project._id + '}',
        signature: '',
        redirect_url: 'https://localhost',
        callback_url: 'https://localhost/transaction/api/transactionCallbackInfo',
        is_test: true
    };
    var options = {
        url: settings.walletAddressGetCreateInvoice,
        method: 'POST',
        headers: headers,
        body: JSON.stringify(walletData)
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            // Print out the response body

            var newTransaction = new Transaction({
                userId: user._id,
                projectId: project._id,
                walletPrice: walletData.price,
                walletCurrency: walletData.currency,
                walletData: walletData.data,
                walletStatus: 'new',
                walletDetailTransaction: 'Add New Project, name: ' + project.name,      //"vote"  OR  "addProject"
                walletInvoice_id: 'body.id',
                walletPayment_url: 'body.payment_url',
                walletSignature: '',
                purposeOfPayment: 'addNewProject'
            });

            //console.log(newTransaction);
            newTransaction.save(function () {
                callback();
            });

        }
    });
};


var parseNameExtensionFile = function (name) {
    var nameSplit = name.split(".");
    var extension = '';
    if (nameSplit.length === 2) {
        return nameSplit[1];
    } else {
        nameSplit = name.split("");
        for (var i = name.length - 1; i > 0; i--) {
            if (nameSplit[i] !== '.') {
                extension += nameSplit[i];
            } else {
                var exNew = '';
                for (var e = extension.length - 1; e > 0; e--) {
                    exNew += extension[e];
                }
                return exNew;
            }
        }
    }
};
