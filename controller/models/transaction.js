/**
 * Created by Mita on 12.6.2017..
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var transactionSchema = new Schema({
    ObjectId: ObjectId,
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    projectId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Project'
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    walletPrice: {type: Number},
    walletCurrency: {type: String},
    walletData: {type: String},
    walletInvoice_id: {
        type: String
    },
    walletStatus: {
        type: String,
        enum: ['new', 'paid', 'confirmed', 'failed', 'expired'],
        default: 'new'
    },
    walletTransactionId: {
        type: String
    },
    walletDetailTransaction: {
        type: String,
        required: true
    },
    walletSignature: {
        type: String
    },
    walletPayment_url: {
        type: String
    },
    purposeOfPayment: {
        type: String,
        enum: ['addNewProject', 'vote']
    },
    dateApproved: {type: Date}
});

// transactionSchema.pre("save", function (done) {
//     var transaction = this;
//     return done(transaction._id);
// });

transactionSchema.methods.setCallBackInfoStatus = function (status, done) {
    this.walletStatus = status;
    if (status === 'confirmed') {
        this.dateApproved = new Date.now();
    }
    this.save(done());
};

var Transaction = mongoose.model("Transaction", transactionSchema);
module.exports = Transaction;