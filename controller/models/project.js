/**
 * Created by Mita on 12.6.2017..
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    settings = require('../settings'),
    fs = require('fs'),
    nodemailer = require('nodemailer'),
    path = require('path');

var projectSchema = new Schema({
    ObjectId: ObjectId,
    userId: {type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User'},
    competitionId: {type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Competition'},
    // groupId: {type: [mongoose.Schema.Types.ObjectId], ref: 'Group'},
    // transactionId: {type: mongoose.Schema.Types.ObjectId, ref: 'Transaction'},
    name: {type: String, required: true},
    createdAt: {type: Date, default: Date.now},
    statusPaid: {type: Boolean, required: true, default: false},
    descriptionFull: {type: String},
    descriptionShort: {type: String},
    youtubeLink: {type: String},
    documentation: {type: String},
    albumImage: {type: [String]},
    directoryNameProject: {type: String},
    voteNumber: {type: Number}
});

projectSchema.pre("save", function (done) {
    return done();
});

projectSchema.methods.setStatus = function (status, user) {
    if (status) {
        this.statusPaid = status;
        var mailOptions = {
            from: settings.transporterMailName + '<' + settings.transporterMail + '>',
            to: user.username,
            subject: 'Info Project Status',
            text: 'Paid success an confirmed, your project is activated!'
            // html: '<a href=\"' + address + '\">' + address + '</a>' // html body
        };
        settings.transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                return console.log(error);
            }
            console.log('Message %s sent: %s', info.messageId, info.response);
        });
        this.save();
    }
};

// projectSchema.methods.setVoteNumber = function (vote, done) {
//     this.voteNumber += vote;
//     this.save(done());
// };

var Project = mongoose.model("Project", projectSchema);
module.exports = Project;