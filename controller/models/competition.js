/**
 * Created by Mita on 12.6.2017..
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var competitionSchema = new Schema({
    ObjectId: ObjectId,
    createdAt: {type: Date, default: Date.now},
    name: {type: String, required: true},
    descriptionCompetition: {type: String, required: true},
    dataStartCompetition: {type: Date, required: true},
    dataStartRevision: {type: Date, required: true},
    dataStartVote: {type: Date, required: true},
    dateEndVote: {type: Date, required: true},
    fondPrice: {type: Number, default: 0},
    status: {
        type: String,
        enum: ['draft', 'active', 'revision', 'vote', 'end'],
        default: 'draft'
    }
});

competitionSchema.pre("save", function (done) {
    return done();
});

competitionSchema.methods.addDays = function (dateObj, numDays, callback) {
    var date = new Date(dateObj);
    this.dateEnd = date.setTime(date.getTime() + (numDays * 24 * 60 * 60 * 1000));
    // return dateObj;
    callback();
};

competitionSchema.methods.setStatus = function (status, done) {
    if (status === 'finished') {
        this.status = 'finished';
        done();
    }
};

competitionSchema.methods.setFondPrice = function (price, done) {
    this.fondPrice += price;
    this.save(done());
};

var Competition = mongoose.model("Competition", competitionSchema);
module.exports = Competition;