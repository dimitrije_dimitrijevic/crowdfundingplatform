/**
 * Created by Mita on 12.6.2017..
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var voteSchema = new Schema({
    ObjectId: ObjectId,
    user: {type: String, required: true},
    competitionId: {type: String, required: true},
    projectId: {type: String, required: true},
    status: {type: Boolean, required: true, default: false},
    createdAt: {type: Date, default: Date.now},
    transactionId: {type: String}
});

voteSchema.pre("save", function (done) {
    var competition = this;
    if (!competition.isModified("dataEnd")) {
        return done();
    }
    voteSchema.addDays(competition.dataEnd, competition.duration, done());
});

voteSchema.methods.addDays = function (dateObj, numDays, callback) {
    dateObj.setDate(dateObj.getDate() + numDays);
    // return dateObj;
    callback();
};

var Vote = mongoose.model("Vote", voteSchema);
module.exports = Vote;