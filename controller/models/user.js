/**
 * Created by Mita on 12.6.2017..
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    nodemailer = require('nodemailer'),
    settings = require('../settings'),
    bcrypt = require("bcrypt"),
    crypto = require('crypto'),
    speakeasy = require('speakeasy'),
    QRCode = require('qrcode');

var SALT_FACTOR = 10;

var userSchema = new Schema({
    ObjectId: ObjectId,
    createdAt: {type: Date, default: Date.now},
    givenName: {type: String, required: true},
    familyName: {type: String, required: true},
    birthday: {type: Date, required: true},
    city: {type: String, required: true},
    state: {type: String, required: true},
    username: {type: String, unique: true, required: true},
    avatarImage: {type: String},
    password: String,
    tel: {type: String, required: true},
    permission: {type: String, default: 'user'},
    resetPasswordToken: {type: String, default: ' '},
    resetPasswordExpires: {type: Date},
    verifyAccount: {type: Boolean, default: false},
    verifyAccountToken: String,
    verifyAccountExpires: Date,
    two_factor_secret: {type: String, default: ''},
    two_factor_enabled: {type: Boolean, default: false},
    displayName: String
});

userSchema.methods.name = function () {
    return this.displayName || this.username;
};

userSchema.methods.newPassword = function (pass, done) {
    var user = this;
    bcrypt.hash(pass, SALT_FACTOR, function (err, hashedPassword) {
        if (err) {
            return done(err);
        }
        user.password = hashedPassword;
        done();
    });
};

userSchema.pre("save", function (done) {
    var user = this;
    if (!user.isModified("username")) {
        return done();
    } else {
        user.newPassword(user.password, done);
        if (!user.verifyAccount) {
            user.createVerifyTokenAccount();
        }
    }
});

userSchema.methods.checkPassword = function (guess, done) {
    bcrypt.compare(guess, this.password, function (err, isMatch) {
        done(err, isMatch);
    });
};

//2fa
userSchema.methods.createTwoFactorSecretKey = function (done) {
    var user = this;

    var secret = speakeasy.generateSecret({length: 20});

    QRCode.toDataURL(secret.otpauth_url, function (err, data_url) {
        // console.log(data_url); // get QR code data URL
        user.two_factor_secret = secret.base32;
        // user.two_factor_enabled = true;
        user.save(function () {
            done(data_url, secret.base32);
        });
    });
};
userSchema.methods.verifyTwoFactor = function (token, done) {
    done(speakeasy.totp.verify({
        secret: this.two_factor_secret,
        encoding: 'base32',
        token: token
    }));
};
//2fa

userSchema.methods.createVerifyTokenAccount = function (callback) {
    var user = this;
    crypto.randomBytes(50, function (err, buf) {
        user.verifyAccountToken = buf.toString('hex');
        user.verifyAccountExpires = new Date().addHours(12);
        var address = settings.urlAddressFrontendVerifyAccountWithMail + '/' + user.username + '/' + user.verifyAccountToken;
        var mailOptions = {
            from: settings.transporterMailName + '<' + settings.transporterMail + '>',
            to: user.username,
            subject: 'Verify your mail',
            text: 'Verify your mail, click on link',
            html: '<a href=\"' + address + '\">' + address + '</a>' // html body
        };
        settings.transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                return console.log(error);
            }
            callback();
            console.log('Message %s sent: %s', info.messageId, info.response);
        });
    });
};

Date.prototype.addHours = function (h) {
    var copiedDate = new Date();
    copiedDate.setTime(this.getTime() + (h * 60 * 60 * 1000));
    return copiedDate;
};

userSchema.methods.checkConfirmEmail = function () {
    return this.verifyAccount;
};

var User = mongoose.model("User", userSchema);
module.exports = User;