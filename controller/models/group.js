/**
 * Created by Mita on 12.6.2017..
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var groupSchema = new Schema({
    ObjectId: ObjectId,
    createdAt: {type: Date, default: Date.now},
    name: {type: String, required: true},
    description: {type: String}
});


var Group = mongoose.model("Group", groupSchema);
module.exports = Group;