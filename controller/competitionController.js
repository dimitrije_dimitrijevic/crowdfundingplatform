/**
 * Created by Mita on 13.6.2017..
 */
var mongoose = require('mongoose'),
    jwt = require('jsonwebtoken'),
    settings = require('./settings'),
    assert = require('assert'),
    Competition = require("./models/competition");

exports.addNewCompetition = function (req, res) {
    // console.log(req.body);
    if (req.user && req.user.permission === 'admin') {
        Competition.findOne({name: req.body.name}, function (err, competition) {
            if (err) {
                return res.status(settings.statusCode_Service_Unavailable_ErrorDb).json({
                    error: true,
                    message: 'DB_error',
                    statusCode: settings.statusCode_Service_Unavailable_ErrorDb
                });
            }
            if (competition) {
                return res.status(settings.statusCode_Bad_Request).json({
                    error: true,
                    message: "competition_exists",
                    statusCode: settings.statusCode_Bad_Request
                });
            }
            var newCompetition = new Competition({
                name: req.body.name,
                descriptionCompetition: req.body.descriptionCompetition,
                dataStartCompetition: req.body.dataStartCompetition,
                dataStartRevision: req.body.dataStartRevision,
                dataStartVote: req.body.dataStartVote,
                dateEndVote: req.body.dateEndVote,
                status: req.body.statusCompetition
            });

            newCompetition.save(function () {
                res.status(settings.statusCode_OK).json({
                    error: false,
                    message: "new_competition_created",
                    statusCode: settings.statusCode_OK
                });
            });
        });
    } else {
        res.status(settings.statusCode_Bad_Request).json({
            error: true,
            message: "user_not_valid",
            statusCode: settings.statusCode_Bad_Request
        });
    }
};

//TODO proveriti potrebu da se vrate svi konkursi
exports.getAllCompetition = function (req, res) {
    if (req.user) {
        var cursor = Competition.find().cursor();
        var data = [];
        cursor.on('data', function (doc) {
            data.push(doc);
        });
        cursor.on('close', function () {
            res.status(200).json(data);
        });
    } else {
        res.status(200).json({
            "error": "user_not_valid"
        });
    }
};
