/**
 * Created by Mita on 12.6.2017..
 */
var express = require('express');
var router = express.Router();
var nodemailer = require('nodemailer');
var settings = require('../controller/settings');
var competitionController = require('../controller/competitionController');

/* GET users listing. */
router.get('/', function (req, res) {
    res.send('respond with a resource');
});


router.post('/addCompetition', competitionController.addNewCompetition);
router.get('/listAllCompetition', competitionController.getAllCompetition);


module.exports = router;
