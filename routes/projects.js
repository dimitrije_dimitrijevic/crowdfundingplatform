/**
 * Created by Mita on 20.6.2017..
 */
var express = require('express');
var router = express.Router();
var settings = require('../controller/settings');
var groupController = require('../controller/groupController');
var projectsController = require('../controller/projectController');

/* GET users listing. */
// router.get('/uploadFile', function (req, res) {
//     res.render('upload')
// });

/**
 * CREATE NEW PROJECT
 */
router.post('/addNewProject', projectsController.addNewProject);
router.post('/editProject', projectsController.editProject);
// router.post('/addNewProjectVerify', projectsController.addNewProjectVerify);
/**
 * END CREATE NEW PROJECT
 */


/**
 * RETURN PROJECTS
 */
router.post('/getAllProjects', projectsController.getAllActiveProject);
router.post('/getCurrentProjects', projectsController.getCurrentProject);
router.post('/getAllUserProjects', projectsController.getAllUserProject);
// router.get('/getImage/file:fileName/image:imageName', projectsController.getProjectImage);
/**
 * END RETURN PROJECTS
 */


/**
 * Create Group OR TAG
 **/
router.post('/getAllTags', groupController.getAllGroup);
router.post('/addGroup', groupController.addNewGroup);
/**
 * END Create Group OR TAG
 **/

module.exports = router;
