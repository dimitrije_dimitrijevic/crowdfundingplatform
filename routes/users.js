/**
 * Created by Mita on 12.6.2017..
 */
var express = require('express');
var router = express.Router();
var userController = require('../controller/userController');

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});

router.get("/login", function (req, res) {
    res.send("login");
});
router.post("/login", userController.login);
router.post("/login/verify2fa", userController.twoFactorVerify);
router.post("/login/verify2fa/save2af", userController.twoFactorVerifyDeliverQrCodeSave);
router.post("/login/verify2fa/reset2fa", userController.twoFactorVerifyResetCode);


router.get("/register", function (req, res) {
    res.send("register");
});
router.post("/register", userController.register);
router.post("/registerAvatar", userController.registerAvatarForUser);
router.post("/Identity/ValidateEmail", userController.verifyAccountFromMail);
router.post("/Identity/NewToken/ValidateEmail", userController.sendNewTokenVerifyAccount);


router.post("/password/reset", userController.resetPasswordSendMail);
router.post("/password/reset/newPassword", userController.resetPasswordSetNew);


// router.post("/token", userController.testToken);
module.exports = router;
