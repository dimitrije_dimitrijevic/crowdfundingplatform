/**
 * Created by Mita on 21.6.2017..
 */
var express = require('express');
var router = express.Router();

var settings = require('../controller/settings');
var transactionController = require('../controller/transactionController');

var request = require('request');

router.post('/api/transactionCallbackInfo', transactionController.walletCallbackInfo);

router.post('/getAllCurrentTransaction', transactionController.getAllCurrentTransaction);

module.exports = router;
